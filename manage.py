#!/usr/bin/env python
import os
import sys
from django.conf import settings

if __name__ == "__main__":
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cavs.settings")

	from cavs.autodiscover import Autodiscover
	autodiscover = Autodiscover()
	autodiscover.apps()
	autodiscover.cache_hooks()

	from core.lib import library
	print library.hook_cache

	print settings.INSTALLED_APPS

	from django.core.management import execute_from_command_line
	execute_from_command_line(sys.argv)
