from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.conf import settings
from core.models import SubcaseStudy, SubcaseTags
# Create your models here.

class TeachingCaseChallenge(models.Model):
	tags = models.ManyToManyField(SubcaseTags)
	subcase = models.ForeignKey(SubcaseStudy)
	challenge = models.TextField(default='')
	description = models.TextField(default='')
	section_key = models.CharField(max_length=50, default='')
	well_key = models.CharField(max_length=50, default='')
	is_active = models.BooleanField(default=True)

	class Meta:
		verbose_name = _('TeachingCaseChallenge')
		verbose_name_plural = _('TeachingCaseChallenges')

	def __unicode__(self):
		pass


class TeachingCaseDilemmaAction(models.Model):
	challenge = models.ForeignKey(TeachingCaseChallenge)
	dilemma = models.TextField(default='')
	action = models.TextField(default='', blank=True)
	question = models.TextField(default='', blank=True)
	dilemma_key = models.CharField(max_length=50,default='')
	answer = models.TextField(default='')
	remark = models.TextField(default='')
	is_active = models.BooleanField(default=True)
    
	class Meta:
		verbose_name = _('TeachingCaseDilemmaAction')
		verbose_name_plural = _('TeachingCaseDilemmaActions')

	def __unicode__(self):
		pass



class TeachingCaseChoice(models.Model):
	challenge = models.ForeignKey(TeachingCaseChallenge)
	choice_key = models.CharField(max_length=50,default='')
	choice = models.TextField(default='')
	is_active = models.BooleanField(default=True)
	status = models.BooleanField(default=False)

	class Meta:
		verbose_name = _('TeachingCaseChoice')
		verbose_name_plural = _('TeachingCaseChoices')

	def __unicode__(self):
		pass


class TeachingCaseQuestion(models.Model):
	challenge = models.ForeignKey(TeachingCaseChallenge)
	question_key = models.CharField(max_length=50,default='')
	question = models.TextField(default='')
	is_active = models.BooleanField(default=True)

	class Meta:
		verbose_name = _('TeachingCaseQuestion')
		verbose_name_plural = _('TeachingCaseQuestions')

	def __unicode__(self):
		pass

class TeachingCaseAnswer(models.Model):
	question = models.ForeignKey(TeachingCaseQuestion)
	user = models.ForeignKey(User)
	answer = models.TextField(default='')
	remark = models.TextField(default='')
	
	class Meta:
		verbose_name = _('TeachingCaseAnswer')
		verbose_name_plural = _('TeachingCaseAnswers')

	def __unicode__(self):
		pass

