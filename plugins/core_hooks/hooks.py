from core.lib.hooks import *
from core.lib.library import Library
from django.conf import settings

library = Library()

class MyHook(SubcaseHook):
	"""docstring for MyHook"""

	templates = [
		{
			'name' : "One",
			'include' : ["one_demo.html",]
		},
		{
			'name' : "Two",
			'include' : ["two_demo.html",]
		},
		{
			'name' : "Three",
			'include' : ["three_demo.html",]
		}
	]

	def __init__(self):
		super(MyHook, self).__init__()

	def __str__(self):
		return u"Test Subcase"



class MyHook2(SubcaseHook):
	"""docstring for MyHook"""

	_js_paths = [settings.STATIC_URL + "core_hooks/js/core_hooks.js",]
	stylesheets = [settings.STATIC_URL + "core_hooks/css/core_hooks.css"]

	templates = [
		{
			'name' : "Design strategic Solution",
			'include' : {
				"id-design-strategic-solution-section" : "part_templates/design_section.html",
			}
		},
		{
			'name' : "Read between the lines",
			'include' : {
				"id-read-between-the-lines-section" : "part_templates/read_btn_lines.html",
			}
		},
		{
			'name' : "Description",
			'include' : {
				"id-description-section" : "part_templates/descriptive_section.html",
			}
		},
		{
			'name' : "Select Solution",
			'include' : {
				"id-select-solution-section" : "part_templates/select_solution_section.html",
			}
		},
	]

	def __init__(self):
		super(MyHook2, self).__init__()

	def __str__(self):
		return u"Teaching"




class ActorTab(TagsHook):

	def __init__(self):
		super(ActorTab, self).__init__()

	def __str__(self):
		return u"Actor"
		
class DecisionPointTab(TagsHook):

	def __init__(self):
		super(DecisionPointTab, self).__init__()

	def __str__(self):
		return u"Decision point"

class EventTab(TagsHook):

	def __init__(self):
		super(EventTab, self).__init__()

	def __str__(self):
		return u"Event"


class SubcaseActorTab(SubcaseTagsHook):

	def __init__(self):
		super(SubcaseActorTab, self).__init__()

	def __str__(self):
		return u"Actor"
		
class SubcaseDecisionPointTab(SubcaseTagsHook):

	def __init__(self):
		super(SubcaseDecisionPointTab, self).__init__()

	def __str__(self):
		return u"Decision point"

class SubcaseEventTab(SubcaseTagsHook):

	def __init__(self):
		super(SubcaseEventTab, self).__init__()

	def __str__(self):
		return u"Event"


class ActorSubcaseTagOptionEditInfo(SubcaseTagObjectOptionHook):
	"""docstring for ActorTagOptionEditInfo"""

	_hook_key = "actor-subcase-tag-option"
	_classname = "actor_subcase_edit_info"
	_js_callback = 'actor_subcase_edit_callback'

	def __init__(self):
		super(ActorSubcaseTagOptionEditInfo, self).__init__()
		
	def __str__(self):
		return "Edit"




# Registration area
library.register(MyHook)
library.register(MyHook2)

library.register(ActorTab)
library.register(DecisionPointTab)
library.register(EventTab)

library.register(SubcaseActorTab)
library.register(SubcaseDecisionPointTab)
library.register(SubcaseEventTab)

library.register(ActorSubcaseTagOptionEditInfo)