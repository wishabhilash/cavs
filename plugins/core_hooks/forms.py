from django import forms

class SubcaseDataForm(forms.Form):
    # TODO: Define form fields here
    data = forms.CharField()
    subcase_id = forms.CharField()

class SubcaseAnswersForm(forms.Form):
    # TODO: Define form fields here
    data = forms.CharField()
    user_id = forms.CharField()
    subcase_id = forms.CharField()


class SaveCommentsForm(forms.Form):
    # TODO: Define form fields here
    data = forms.CharField()
    user_id = forms.CharField()
    
class ChallengeIdentityForm(forms.Form):
    subcase_id = forms.CharField()
    section_key = forms.CharField()
    challenge_key = forms.CharField()
    tag_id = forms.CharField(required=False)
