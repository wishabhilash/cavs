# Create your views here.

from django.conf import settings
from core.lib.decorators import login_required, ajax_view, authorize_only_moderators
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
import json
from core.lib.decorators import auth_required_view_case, auth_required_view_subcase
from core.models import *
from core import views as core_views
from core.lib import library
from .models import *


@auth_required_view_subcase(redirect_url = "/plugins/core_hooks/view_subcase/")
@login_required
def view_subcase(request, *args, **kwargs):
	return view_subcase_unauth(request, *args, **kwargs)

def view_subcase_unauth(request, *args, **kwargs):
	

	subcase = SubcaseStudy.objects.get(id = int(kwargs['subcase_id']))
	subcase_hooks = library.hook_cache['subcase_hook'][subcase.subcase_id]

	# Fetch challenges that are active
	subcase_sections = TeachingCaseChallenge.objects.filter(subcase_id = int(kwargs['subcase_id']), is_active = True)
	output = {}
	for section in subcase_sections:
		well_values = {
			'section_challenge' : section.challenge,
			'section_description' : section.description,
		}

		# Get the Questions
		questions = TeachingCaseQuestion.objects.filter(challenge_id = section.id, is_active = True)
		for question in questions:
			well_values[question.question_key] = question.question


		# Get the Choices
		choices = TeachingCaseChoice.objects.filter(challenge_id = section.id, is_active = True)
		for choice in choices:
			well_values[choice.choice_key] = [choice.choice,choice.status]


		# Get the dilemmas
		dilemmas = TeachingCaseDilemmaAction.objects.filter(challenge_id = section.id, is_active = True)
		for dilemma in dilemmas:
			well_values[dilemma.dilemma_key] = [dilemma.dilemma, dilemma.action, dilemma.question]

		try:
			output[section.section_key].update({section.well_key : well_values})
		except KeyError:
			output[section.section_key] = {section.well_key : well_values}
	
	kwargs['case_name'] = CaseStudy.objects.only('name').get(pk=subcase.case_id_id).name
	kwargs['subcase_name'] = subcase.name	
	kwargs['view_mode'] = True
	kwargs['subcase_data'] = output
	kwargs['case_id'] = subcase.case_id_id
	kwargs['story'] = subcase.story
	kwargs['js_includes'] = subcase_hooks.get_js_paths()
	kwargs['css_includes'] = subcase_hooks.get_css_paths()
	kwargs['sections'] = subcase_hooks.templates[subcase.template_id]['include']

	# Get all the sections
	kwargs['all_sections'] = {}
	for value in subcase_hooks.templates.itervalues():
		kwargs['all_sections'].update(value['include'])

	# Retrieve and pass templates associated with this subcase to template
	kwargs['section_menu_items'] = [section_menu_item['name'] for section_menu_item in subcase_hooks.templates.itervalues() if section_menu_item['include'] != kwargs['sections']]
	
	kwargs['subcase_tags_obj'] = [hook_obj for hook_obj in library.hook_cache['subcase_tags_hook'].itervalues()]

	return render_to_response("edit_subcase.html", kwargs, context_instance = RequestContext(request))



@login_required
def solve_subcase(request, *args, **kwargs):
	# Retrieve the subcase

	subcase = SubcaseStudy.objects.get(id = int(kwargs['subcase_id']))
	subcase_hooks = library.hook_cache['subcase_hook'][subcase.subcase_id]

	# Fetch challenges that are active
	subcase_sections = TeachingCaseChallenge.objects.filter(subcase_id = int(kwargs['subcase_id']), is_active = True)
	output = {}
	for section in subcase_sections:
		well_values = {
			'section_challenge' : section.challenge,
			'section_description' : section.description,
		}
		questions = TeachingCaseQuestion.objects.filter(challenge_id = section.id, is_active = True)

		# Insert all the questions associated with the well
		for question in questions:
			well_values[question.question_key] = question.question
			try:
				answer = TeachingCaseAnswer.objects.only('answer').get(question_id = question.id, user = request.user)
				well_values["answer_" + question.question_key.split("_")[-1]] = answer.answer
			except Exception, e:
				well_values["answer_" + question.question_key.split("_")[-1]] = ''


		# Get the Choices
		choices = TeachingCaseChoice.objects.filter(challenge_id = section.id, is_active = True)
		for choice in choices:
			well_values[choice.choice_key] = [choice.choice,choice.status]


		# Get the dilemmas
		dilemmas = TeachingCaseDilemmaAction.objects.filter(challenge_id = section.id, is_active = True)
		for dilemma in dilemmas:
			well_values[dilemma.dilemma_key] = [dilemma.dilemma, dilemma.action, dilemma.question, dilemma.answer, dilemma.remark]


		try:
			output[section.section_key].update({section.well_key : well_values})
		except KeyError:
			output[section.section_key] = {section.well_key : well_values}
	
	kwargs['solve_mode'] = True
	kwargs['subcase_name'] = subcase.name
	kwargs['subcase_data'] = output
	kwargs['case_id'] = subcase.case_id_id
	kwargs['story'] = subcase.story
	kwargs['js_includes'] = subcase_hooks.get_js_paths()
	kwargs['css_includes'] = subcase_hooks.get_css_paths()
	kwargs['sections'] = subcase_hooks.templates[subcase.template_id]['include']

	# Get all the sections
	kwargs['all_sections'] = {}
	for value in subcase_hooks.templates.itervalues():
		kwargs['all_sections'].update(value['include'])

	# Retrieve and pass templates associated with this subcase to template
	kwargs['section_menu_items'] = [section_menu_item['name'] for section_menu_item in subcase_hooks.templates.itervalues() if section_menu_item['include'] != kwargs['sections']]
	
	kwargs['subcase_tags_obj'] = [hook_obj for hook_obj in library.hook_cache['subcase_tags_hook'].itervalues()]


	return render_to_response("solve_subcase.html", kwargs, context_instance = RequestContext(request))



USERS_WHO_ANSWERED_CACHE = []

@authorize_only_moderators
@login_required
def evaluate_case_study(request, *args, **kwargs):
	users_answered = set()
	case_id = int(kwargs['case_id'])
	case = CaseStudy.objects.get(pk=case_id, is_active=True)
	subcases = SubcaseStudy.objects.filter(case_id = case, is_active = True)
	for subcase in subcases:
		challenges = TeachingCaseChallenge.objects.filter(subcase = subcase, is_active=True)
		for challenge in challenges:
			questions = TeachingCaseQuestion.objects.filter(challenge=challenge, is_active=True)
			for question in questions:
				answers = TeachingCaseAnswer.objects.filter(question=question)
				for answer in answers:
					users_answered.add(answer.user)

	users_answered = list(users_answered)

	global USERS_WHO_ANSWERED_CACHE
	USERS_WHO_ANSWERED_CACHE = [int(user.id) for user in users_answered]

	kwargs['case_name'] = case.name
	kwargs['users_answered'] = users_answered
	return render_to_response("evaluate_case_study.html", kwargs, context_instance = RequestContext(request))



@authorize_only_moderators
@login_required
def evaluate_answers(request, *args, **kwargs):
	def get_prev_next_users(user_id, id_list):
		'''
		Given the user_id, it returns the previous and next user_id's in a dict
		'''
		ret_dat = {}
		
		index = id_list.index(user_id)
		if index == 0:
			ret_dat['prev'] = None
		else:
			ret_dat['prev'] = id_list[index-1]

		if index == len(id_list)-1:
			ret_dat['next'] = None
		else:
			ret_dat['next'] = id_list[index+1]

		return ret_dat

	global USERS_WHO_ANSWERED_CACHE
	print USERS_WHO_ANSWERED_CACHE

	user_id = int(kwargs['user_id'])
	prev_next_users = get_prev_next_users(user_id, USERS_WHO_ANSWERED_CACHE)
	js_includes = set()
	css_includes = set()
	subcases = SubcaseStudy.objects.filter(case_id = int(kwargs['case_id']), is_active=True)
		
	
	# Fetch challenges that are active
	output = {}
	for subcase in subcases:
		subcase_hooks = library.hook_cache['subcase_hook'][subcase.subcase_id]

		# Collect all js paths
		for path in subcase_hooks.get_js_paths():
			js_includes.add(path)

		# Collect all css paths
		for path in subcase_hooks.get_css_paths():
			css_includes.add(path)

		output[subcase.id] = {}
		subcase_sections = TeachingCaseChallenge.objects.filter(subcase_id = int(subcase.id), is_active = True)
		for section in subcase_sections:
			well_values = {
				'section_challenge' : section.challenge,
				'section_description' : section.description,
			}
			questions = TeachingCaseQuestion.objects.filter(challenge_id = section.id, is_active = True)

			# Insert all the questions associated with the well
			for question in questions:
				well_values[question.question_key] = question.question

				# Get all the answers and comments
				try:
					answer = TeachingCaseAnswer.objects.only('answer', 'remark').get(question_id = question.id, user = user_id)
					well_values["answer_" + question.question_key.split("_")[-1]] = answer.answer
					well_values["comment_" + question.question_key.split("_")[-1]] = answer.remark
				except Exception, e:
					well_values["answer_" + question.question_key.split("_")[-1]] = ''
					well_values["comment_" + question.question_key.split("_")[-1]] = ''

			choices = TeachingCaseChoice.objects.filter(challenge_id = section.id, is_active = True)

			for choice in choices:
				well_values[choice.choice_key] = [choice.choice,choice.status]

			# Get the dilemmas
			dilemmas = TeachingCaseDilemmaAction.objects.filter(challenge_id = section.id, is_active = True)
			for dilemma in dilemmas:
				well_values[dilemma.dilemma_key] = [dilemma.dilemma, dilemma.action, dilemma.question, dilemma.answer, dilemma.remark]
			
			try:
				output[subcase.id][section.section_key].update({section.well_key : well_values})
			except KeyError:
				output[subcase.id][section.section_key] = {section.well_key : well_values}
	
	kwargs['prev_user_id'] = prev_next_users['prev']
	kwargs['next_user_id'] = prev_next_users['next']
	kwargs['evaluation_mode'] = True
	kwargs['subcase_name'] = subcase.name
	kwargs['subcase_data'] = output
	kwargs['story'] = subcase.story
	kwargs['js_includes'] = list(js_includes)
	kwargs['css_includes'] = list(css_includes)
	kwargs['sections'] = subcase_hooks.templates[subcase.template_id]['include']

	# Get all the sections
	kwargs['all_sections'] = {}
	for value in subcase_hooks.templates.itervalues():
		kwargs['all_sections'].update(value['include'])

	# Retrieve and pass templates associated with this subcase to template
	kwargs['section_menu_items'] = [section_menu_item['name'] for section_menu_item in subcase_hooks.templates.itervalues() if section_menu_item['include'] != kwargs['sections']]
	
	kwargs['subcase_tags_obj'] = [hook_obj for hook_obj in library.hook_cache['subcase_tags_hook'].itervalues()]

	return render_to_response("evaluate_answers.html", kwargs, context_instance = RequestContext(request))


def case_study_search(request, *args, **kwargs):
	'''
	Function for returning back results for search term
	'''
	