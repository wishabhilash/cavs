from django import template
from core.models import *
from collections import OrderedDict

register = template.Library()


@register.filter
def can_edit_subcase(value,arg):
	'''
	Verifies if the user has the preveliges to actually edit the case. If yes, True is returned else False
	'''
	try:
		CaseStudy.objects.get(moderator__id__exact = int(value), pk=int(arg))
		return True
	except Exception, e:
		try:
			CaseStudy.objects.get(collaborator__id__exact = int(value), pk=int(arg))
			return True
		except Exception, e:
			return False

@register.filter
def get_answer_id(value):
	return value.split('_')[-1]


@register.filter
def get_answer(question, data):
	return data["answer_"+question.split("_")[-1]]

@register.filter
def get_comment(question, data):
	# print data
	# print "comment_"+question.split("_")[-1]
	return data["comment_"+question.split("_")[-1]]


@register.filter
def compute_well_path(value):
	value_list = value.split(".")
	value_list[0] += "_well"
	return ".".join(value_list)

@register.filter
def get_user_name(value):
	user = User.objects.get(pk=int(value))
	return user.first_name + " " + user.last_name

@register.filter
def get_case_name(value):
	return CaseStudy.objects.get(pk=int(value), is_active=True).name
