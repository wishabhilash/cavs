// This file consists of all the JS required for the plugin to work
// for rest of JS look for file with name edit_subcase.js



$(function(){

	// Supplimentary function to hide context menu when clicked anywhere
	// else other than on the menu
    $("body").mouseup(function(event){
		// Hide the section cog-menu when clicked anywhere on the page
		$(".sectioncontrol-btns-small .dropdown").addClass('hide');
	});



    // Add + Challenge menu item to all subcase sections
    if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True'){
	    $(".section").each(function(){
			$(this).not("#story-section, #tags-block").each(function(){
				$(this).find(".sectioncontrol-btns-small li").append('<a href="javascript:void(0)" role="menuitem" class="add-challenge"><i class="iconic-plus"></i> Challenge</a>');
			})
		})
    }
		

	// Array Default section in the subcase - needed to add delete section menu item to no default sections
	var default_sections_in_subcase = new Array();
	$(".subcase-default-sections").each(function(){
		default_sections_in_subcase.push($(this).val());
	})

	// Add cog-menu item <Delete section> except for the default sections - they should not be deleted since they define the template itself; also add a modal for it
    if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True'){

		$(".sectioncontrol-btns-small li").each(function(){
			if($(this).parents(".section").attr('id') === "story-section" || $(this).parents(".section").attr('id') === 'tags-block') return;
			var data_id = $(this).parents('.section').attr('id');
			if($.inArray(data_id, default_sections_in_subcase) && data_id !== ''){

				// Add section specific modal to body
				var section_delete_modal = $("#delete-section-confirmation-modal").clone().attr('id',data_id + "-modal");
				$("body").append(section_delete_modal);

				// Add cog-menu delete section item
				$(this).append('<a href="#' + data_id + "-modal" + '" role="menuitem" class="delete-section" title="Delete Section" data-toggle="modal"><i class="iconic-minus"></i> Delete Section</a>');

				// Activate modal delete confirmation action
				$("#" + data_id + "-modal").find(".modal-footer .btn-danger").click(delete_section_action_yes);
			}
		})
	}
	
	
	// If its not evaluation mode, then only add challenge delete button
	if ($("#this-subcase-evaluation-mode").val() !== "True" && $("#this-subcase-display-mode").val() !== "True" && $("#this-subcase-solve-mode").val() !== "True") {
		// Add delete challenge buttons to all the challenges more than 1 on load
		var well_challenge_sections = $(".well").not('#well_0').find(".section-challenge");
		$(well_challenge_sections).append(create_delete_challenge_btn());
		$(well_challenge_sections).on('click', ".delete-challenge-btn",delete_challenge_action);
	}
	

	// On click show the graph for relations
	$(".relations-challenge-btn button").click(show_challenge_relations);


/////////////////////////  ADD REMOVE BUTTONS AND ACTIVATE   ////////////////////////////////////


	// Add question delete button to questions except to questions having id=question_0 on load
	if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True') $(".section-question").append(create_question_remove_btn());

	// On click - beside question row, show confirm dialog on load
	if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True') $(".remove-question-btn").on('click','button.btn-danger',show_question_delete_confirm_dialog);





	// Add choice delete button to choices except to choices having id=choice_0 or choice_1 on load
	if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True') $('.section-choice').append(create_choice_remove_btn());

	// // On click - beside choice row, show confirm dialog on load
	if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True') $(".remove-choice-btn").on('click','button.btn-danger',show_choice_delete_confirm_dialog);


	// Add dilemma delete button to questions except to questions having id=question_0 on load
	if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True') $(".section-dilemma[id^='dilemma_dilemma_']").append(create_dilemma_remove_btn());

	// On click - beside question row, show confirm dialog on load
	if ($('#this-subcase-display-mode').val() !== 'True' && $('#this-subcase-solve-mode').val() !== 'True' && $('#this-subcase-evaluation-mode').val() !== 'True') $(".remove-dilemma-btn[id^='dilemma_dilemma_']").on('click','button.btn-danger',show_dilemma_delete_confirm_dialog);


/////////////////////////////////////////////////////////////////////////////////////////



    // Refresh codes on initiation
    autosize_textareas(".section-content .well textarea");

	
	// Activate all textareas so that on change well state is altered=true on load
	$('textarea').change(mark_challenge_altered);

	
	// When mouse enters a question the following events occur
	$(".section-content").on('mouseenter', '.well div',mouse_enter_row);

	// When mouse leaves a question the following events occur
	$(".section-content").on('mouseleave', '.well div',mouse_leave_row);

	// Add a question when + is clicked
	$(".add-question-btn").on('click','button.btn-success',add_question);

	// On click - beside question row, show confirm dialog
	$(".remove-question-btn").on('click','button.btn-danger',show_question_delete_confirm_dialog);

	// Add a question when + is clicked
	$(".add-choice-btn").on('click','button.btn-success',add_choice);


	// On click - beside question row, show confirm dialog
	$(".remove-choice-btn").on('click','button.btn-danger',show_choice_delete_confirm_dialog);


	// Add a dilemma set when + is clicked
	$(".add-dilemma-btn").on('click','button.btn-success',add_dilemma);

	// On click - beside question row, show confirm dialog
	$(".remove-dilemma-btn").on('click','button.btn-danger',show_dilemma_delete_confirm_dialog);


	
	
	// Activate all add_challenge buttons by default
	$(".add-challenge").click(add_challenge);
	
	// Activate selectize for tags
	if ($("#this-subcase-evaluation-mode").val() !== "True"){
		$(".well").each(function(index, value){
			activate_selectize(value);
		})
		
	} 


	// If this-subcase-display-mode == True, this must be used to darken 
	// text color of disabled elements
	if ($('#this-subcase-display-mode').val() === 'True' || $('#this-subcase-solve-mode').val() === 'True' || $('#this-subcase-evaluation-mode').val() === 'True') $('textarea').each(function(){
		if($(this).attr('disabled') === 'disabled') $(this).css('color', '#6b6b6b');
	})


	if ($('#this-subcase-display-mode').val() === 'True'){
		$("#id-case-name-info .solve_subcase").click(function(){
			window.location = "/plugins/core_hooks/solve_subcase/" + $("#this-subcase-id").val() + "/";
		})
	}
});


// Get the first letter of each word capitalized
function get_capitalized_string(str, delimiter){
	if(!delimiter) delimiter = '';
	var split_str = str.split(delimiter);
	for(var i = 0; i < split_str.length; i++){
		split_str[i] = split_str[i].charAt(0).toUpperCase() + split_str[i].slice(1);
	}
	return split_str.join(' ');
}



// Background on clicking Show Relations button
function show_challenge_relations(event){
	
	$("#relations-overlay").removeClass("hide");

	$("#relations-overlay .modal-backdrop-cancel").click(function(){
		$(this).parents(".modal-backdrop").addClass("hide");
		$("#graph-site").remove();
	});

	var challenge_key = $(this).parents(".well").attr('id'),
	section_key = $(this).parents(".section").attr('id'),
	subcase_id = $("#this-subcase-id").val();

	if (!(challenge_key && section_key && subcase_id)) return;

	$.ajax({
		url : "/plugins/core_hooks/fetch_challenge_tags/",
		method : 'post',
		context : this,
		data : {
			subcase_id : subcase_id,
			challenge_key : challenge_key,
			section_key : section_key
		},
		dataType : 'json',
		success : function(data){
			var nodes = {}, edges = {};
			var root_node = $(this).parents(".well").find(".section-challenge textarea").text();
			nodes[root_node] = {color:"red", shape:"dot", alpha:1};
			edges[root_node] = {};

			var CLR = {
				branch:"#55A11A",
				code:"orange",
				doc:"#922E00",
				demo:"#00AF8E"
			}

			// console.log(nodes);
			$(data.tags).each(function(index, value){
				var tab_node = get_capitalized_string(value.type.substring(0,value.type.length - "-tab-pane".length), '-');
				nodes[tab_node] = {color:CLR.branch, shape:"dot", alpha:1};
				nodes[value.data] = {color:CLR.doc, alpha:0, link:'#'};

				edges[root_node][tab_node] = {length:1};

				if(typeof edges[tab_node] == 'undefined'){
					edges[tab_node] = {};
				}
				edges[tab_node][value.data] = {length:1};
				
			})

			var newUI = {
				nodes : nodes,
				edges : edges
			}


			$("#relations-overlay").append('<canvas id="graph-site"></canvas>');

			var sys = arbor.ParticleSystem();
			sys.parameters({stiffness:900, repulsion:2000, gravity:true, dt:0.015});
			sys.renderer = Renderer("#graph-site", newUI.edges);
			sys.graft(newUI);

		},
		error : function(xhr, status, error){

		}
	})

	

}


function activate_selectize(well){
	var selector = $(well).find(".select-tags");
	$.ajax({
		url : "/plugins/core_hooks/fetch_challenge_tags/",
		method : 'post',
		data : {
			subcase_id : $("#this-subcase-id").val(),
			section_key : $(well).parents(".section").attr('id'),
			challenge_key : $(well).attr('id')
		},
		dataType : 'json',
		success : function(data){
			if (data.tags) {
				$.each(data.tags, function(index, value){
					$(well).find(".select-tags").append($('<option value="' + value.id + '" data-data="{&quot;data&quot;:&quot;' + value.data + '&quot;}" selected="selected">' + value.data + '</option>'));
				})
			}
			

			// Populate tags for selectize
			$(selector).selectize({
				plugins : ['remove_button'],
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				create: false,
				render: {
					option: function(item, escape) {
						return '<div>' + item.name + '</div>';
					}
				},
				load : function(query, callback){
					if(!query.length) return callback();

					$.ajax({
						url : "/plugins/core_hooks/fetch_subcase_tags/",
						method : 'post',
						dataType : 'json',
						data : {
							subcase_id : $('#this-subcase-id').val(),
							data : query,
						},
						success : function(data){
							callback(data.tags);
						},
						error : function(xhr, status, error){
							callback();
						}
					});
				},
				onItemAdd : function(value, item){
					$.ajax({
						url : '/plugins/core_hooks/add_challenge_tags/',
						method : 'post',
						context : this,
						data : {
							subcase_id : $("#this-subcase-id").val(),
							section_key : $(well).parents(".section").attr('id'),
							challenge_key : $(well).attr('id'),
							tag_id : value
						},
						dataType : 'json',
						success : function(data){
							console.log(data);
						},
						error : function(xhr, status, error){
							var control = this.$input[0].selectize;
							control.removeItem(value);
							control.refreshOptions();
							notify("Error", "Some error happened while adding the collaborator. Try again in some time.", 'danger', {nonblock:true});
						}
					})
				},
				onItemRemove : function(value){
					$.ajax({
						url : "/plugins/core_hooks/delete_challenge_tags/",
						method : 'post',
						data : {
							subcase_id : $("#this-subcase-id").val(),
							section_key : $(well).parents(".section").attr('id'),
							challenge_key : $(well).attr('id'),
							tag_id : value
						},
						dataType : 'json',
						success : function(data){
							console.log(data);
						},
						error : function(xhr, status, error){
							var control = this.$input[0].selectize;
							control.removeItem(value);
							control.refreshOptions();
							notify("Error", "Some error happened while removing the collaborator. Try again in some time.", 'danger', {nonblock:true});
						}
					})
				}

				
			});
		},
		error : function(xhr, status, error){

		}
	})

	

}

// Resize the textarea as the content grows
function autosize_textareas(selector){
	$(selector).autosize();
}


////////////////////////////////// QUESTIONS LOGIC ///////////////////////////////////////////////////
// Add a question
function add_question(event){
	if ($('#this-subcase-display-mode').val() === 'True' || $('#this-subcase-solve-mode').val() === 'True' || $('#this-subcase-evaluation-mode').val() === 'True') return false;
	var section_question = $(this).parents(".editable-div").clone();
	section_question = $(section_question[0]);
	section_question.removeClass("default-question").addClass("section-question").attr('id', 'question_' + get_question_id(this));
	section_question.find("textarea").val('');
	section_question.find("span.floating-label").css('display', 'block');
	section_question.find(".add-question-btn").remove();

	// Add a remove question button on adding a question
	section_question.append(create_question_remove_btn());

	$(section_question).find('textarea').css("height","20px");

	// Enable autoresizing of row
	autosize_textareas($(section_question).find("textarea"));

	$($(this).parents(".editable-div").parent().find('[class$="-question"]:last')).after(section_question);

	// On click - beside question row, show confirm dialog
	$(".remove-question-btn").on('click','button.btn-danger',show_question_delete_confirm_dialog);

	$(section_question).find('textarea').change(mark_challenge_altered);


}



// Increment the id of a new choice based on the id of the last question in a challenge
function get_question_id(selector){
	return (parseInt($(selector).parents(".editable-div").parent().find('[class$="-question"]:last').attr('id').split('_')[1]) + 1);
}

function question_delete_confirm(){
	$.ajax({
		url : '/plugins/core_hooks/delete_row/',
		method : 'post',
		context : this,
		data : {
			subcase_id : $("#this-subcase-id").val(),
			data : $(this).parents(".section").attr('id') + ":" + $(this).parents('.well').attr('id') + ":" + $(this).parent().parent().attr('id')
		},
		success : function(data){
			// Remove the question from DOM
			$(this).parents(".editable-div").remove();
		},
		error : function(xhr, status, error){

		}
	})	

	
}

function question_delete_cancel(){
	var section_question = $(this).parent().parent();
	$(section_question).find(".remove-question-btn").remove();
	$(section_question).find(".delete-row-confirmation").remove();
	var remove_question_btn = $(".remove-question-btn.hide");
	remove_question_btn = $(remove_question_btn[0]).clone().removeClass('hide');
	$(section_question).append(remove_question_btn);

	// On click - beside question row, show confirm dialog
	$(".remove-question-btn").on('click','button.btn-danger',show_question_delete_confirm_dialog);

}

function show_question_delete_confirm_dialog(){
	$(this).parents(".editable-div").append(create_row_delete_confirmation('question'));

	$(".question-delete-confirm").click(question_delete_confirm);

	$(".question-delete-cancel").click(question_delete_cancel);

}


//////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////// DILEMMA SECTION ////////////////////////////////////


function add_dilemma(event){

	if ($('#this-subcase-display-mode').val() === 'True' || $('#this-subcase-solve-mode').val() === 'True' || $('#this-subcase-evaluation-mode').val() === 'True') return false;

	var well = $(this).parents(".well");
	var dilemma = $(well).find("#dilemma_dilemma_0").clone();
	dilemma.find(".add-dilemma-btn").remove();
	dilemma.removeClass("default-dilemma").addClass("section-dilemma").attr('id','dilemma_dilemma_' + get_dilemma_id(this));
	dilemma.find('textarea').val('');
	dilemma.find("span.floating-label").css('display', 'block');

	// Add a remove question button on adding a question
	dilemma.append(create_dilemma_remove_btn());

	$(dilemma).find('textarea').css("height","20px");

	// Enable autoresizing of row
	autosize_textareas($(dilemma).find("textarea"));

	dilemma.find('textarea').change(mark_challenge_altered);



	// Configure the actions
	var action = $(well).find("#dilemma_action_0").clone();
	action.find(".add-dilemma-btn").remove();
	action.removeClass("default-dilemma").addClass("section-dilemma").attr('id','dilemma_action_' + get_dilemma_id(this));
	action.find('textarea').val('');
	action.find("span.floating-label").css('display', 'block');
	action.find('textarea').css("height","20px");

	// Enable autoresizing of row
	autosize_textareas($(action).find("textarea"));

	action.find('textarea').change(mark_challenge_altered);



	// Configure the questions
	var question = $(well).find("#dilemma_question_0").clone();
	question.find(".add-dilemma-btn").remove();
	question.removeClass("default-dilemma").addClass("section-dilemma").attr('id','dilemma_question_' + get_dilemma_id(this));
	question.find('textarea').val('');
	question.find("span.floating-label").css('display', 'block');
	question.find('textarea').css("height","20px");

	// Enable autoresizing of row
	autosize_textareas($(question).find("textarea"));



	question.find('textarea').change(mark_challenge_altered);
	// On click - beside question row, show confirm dialog
	$(".remove-dilemma-btn").on('click','button.btn-danger',show_dilemma_delete_confirm_dialog);


	// Add dilemma, action and question to the DOM just after the last dilemma_question
	$($(this).parents(".editable-div").parent().find('[id^="dilemma_question_"]:last')).after(dilemma);
	$($(this).parents(".editable-div").parent().find('[id^="dilemma_dilemma_"]:last')).after(action);
	$($(this).parents(".editable-div").parent().find('[id^="dilemma_action_"]:last')).after(question);

}

// Increment the id of a new choice based on the id of the last question in a challenge
function get_dilemma_id(selector){
	return (parseInt($(selector).parents(".editable-div").parent().find('[id^="dilemma_dilemma_"]:last').attr('id').split('_')[2]) + 1);
}


function dilemma_delete_confirm(){
	$.ajax({
		url : '/plugins/core_hooks/delete_row/',
		method : 'post',
		context : this,
		data : {
			subcase_id : $("#this-subcase-id").val(),
			data : $(this).parents(".section").attr('id') + ":" + $(this).parents('.well').attr('id') + ":" + $(this).parent().parent().attr('id')
		},
		success : function(data){
			// Remove the question from DOM
			$(this).parents(".editable-div").next().next().remove();
			$(this).parents(".editable-div").next().remove();
			$(this).parents(".editable-div").remove();
		},
		error : function(xhr, status, error){

		}
	})	

	
}

function dilemma_delete_cancel(){
	var section_question = $(this).parent().parent();
	$(section_question).find(".remove-dilemma-btn").remove();
	$(section_question).find(".delete-row-confirmation").remove();
	var remove_question_btn = $(".remove-dilemma-btn.hide");
	remove_question_btn = $(remove_question_btn[0]).clone().removeClass('hide');
	$(section_question).append(remove_question_btn);

	// On click - beside question row, show confirm dialog
	$(".remove-dilemma-btn").on('click','button.btn-danger',show_question_delete_confirm_dialog);

}



function show_dilemma_delete_confirm_dialog(){
	$(this).parents(".editable-div").append(create_row_delete_confirmation('dilemma'));

	$(".dilemma-delete-confirm").click(dilemma_delete_confirm);

	$(".dilemma-delete-cancel").click(dilemma_delete_cancel);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////// CHOICES LOGIC ///////////////////////////////////////////////////
// Add a choice
function add_choice(event){
	var section_choice = $(this).parents(".editable-div").clone();

	section_choice = $(section_choice[0]);

	section_choice.removeClass("default-choice").addClass("section-choice").attr('id', 'choice_' + get_choice_id(this));
	
	section_choice.find("textarea").val('');
	section_choice.find("span.floating-label").css('display', 'block');
	section_choice.find(".add-choice-btn").remove();
	
	section_choice.append(create_choice_remove_btn());

	$(section_choice).find('textarea').css("height","20px");

	// Enable autoresizing of row
	autosize_textareas($(section_choice).find("textarea"));

	$($(this).parents(".editable-div").parent().find('[class$="-choice"]:last')).after(section_choice);


	// On click - beside question row, show confirm dialog
	$(".remove-choice-btn").on('click','button.btn-danger',show_choice_delete_confirm_dialog);

	$(section_choice).find('textarea').change(mark_challenge_altered);
}

// Increment the id of a new choice based on the id of the last choice in a challenge
function get_choice_id(selector){
	return (parseInt($(selector).parents(".editable-div").parent().find('[class$="-choice"]:last').attr('id').split('_')[1]) + 1);
}


function choice_delete_confirm(){
	$.ajax({
		url : '/plugins/core_hooks/delete_row/',
		method : 'post',
		context : this,
		data : {
			subcase_id : $("#this-subcase-id").val(),
			data : $(this).parents(".section").attr('id') + ":" + $(this).parents('.well').attr('id') + ":" + $(this).parent().parent().attr('id')
		},
		success : function(data){
			// Remove the question from DOM
			$(this).parents(".editable-div").remove();
		},
		error : function(xhr, status, error){

		}
	})	

}

function choice_delete_cancel(){
	var section_choice = $(this).parent().parent();
	$(section_choice).find(".remove-choice-btn").remove();
	$(section_choice).find(".delete-row-confirmation").remove();
	var remove_question_btn = $(".remove-question-btn.hide");
	remove_question_btn = $(remove_question_btn[0]).clone().removeClass('hide').removeClass("remove-question-btn").addClass("remove-choice-btn").attr("title","Delete Choice");
	$(section_choice).append(remove_question_btn);

	// On click - beside question row, show confirm dialog
	$(".remove-choice-btn").on('click','button.btn-danger',show_choice_delete_confirm_dialog);

}

function show_choice_delete_confirm_dialog(){
	$(this).parents(".editable-div").append(create_row_delete_confirmation('choice'));

	$(".choice-delete-confirm").click(choice_delete_confirm);

	$(".choice-delete-cancel").click(choice_delete_cancel);

}


//////////////////////////////////////////////////////////////////////////////////////////////////////



// Delete section action: When pressed Yes
function delete_section_action_yes(){
	// Exit the modal
	$(this).siblings(".btn-primary").trigger('click');

	// Also remove the section modal
	$(this).parent().parent().remove();

	var section_id = $(this).parent().parent().attr('id').slice(0,-6);
	$.ajax({
		url : "/plugins/core_hooks/delete_section/",
		method : 'post',
		context : this,
		data : {
			data : section_id,
			subcase_id : $("#this-subcase-id").val()
		},
		success : function(data){
			if (data === "1") {
				// Remove the section
				$("#" + section_id).remove();
				// Enable the add section item again
				$("#id-contextmenu-template").find("#" + $(this).parent().parent().attr('id').slice(0,-14)).parent().removeClass('disabled').click(return_true);
				notify("Success", "Section deleted successfully.", "success", {nonblock : true});
			}
		},
		error : function(xhr, status, error){
			notify("Error deleting section", "Unable to delete section. Try again in some time.", "danger", {nonblock : true});
		}
	})
	
	
}



// Events happening on mouse entering the row
function mouse_enter_row(){
	var element = $(this).find("span.floating-label");
	element.fadeOut(200);
	if ($(this).hasClass('section-choice')) {
		var remove_width = $(this).find('.remove-choice-btn').width()
		$($(this).find('.remove-choice-btn')).animate({
			right : "-=" + (remove_width + 6),
			opacity : 1
		}, 100)
	};
	
	if ($(this).hasClass('section-question')) {
		var remove_width = $(this).find('.remove-question-btn').width()
		$($(this).find('.remove-question-btn')).animate({
			right : "-=" + (remove_width + 6),
			opacity : 1
		}, 100)
	};

	if ($(this).hasClass('section-dilemma')) {
		var remove_width = $(this).find('.remove-dilemma-btn').width()
		$($(this).find('.remove-dilemma-btn')).animate({
			right : "-=" + (remove_width + 6),
			opacity : 1
		}, 100)
	};

}

// Events happening on mouse leaving the row
function mouse_leave_row(){
	var element = $(this).find("span.floating-label");
	element.fadeIn(200);
	if ($(this).hasClass('section-choice')) {
		var remove_width = $(this).find('.remove-choice-btn').width()
		$($(this).find('.remove-choice-btn')).animate({
			right : "+=" + (remove_width + 6),
			opacity : 0
		}, 100)
	};

	if ($(this).hasClass('section-question')) {
		var remove_width = $(this).find('.remove-question-btn').width()
		$($(this).find('.remove-question-btn')).animate({
			right : "+=" + (remove_width + 6),
			opacity : 0
		}, 100)
	};

	if ($(this).hasClass('section-dilemma')) {
		var remove_width = $(this).find('.remove-dilemma-btn').width()
		$($(this).find('.remove-dilemma-btn')).animate({
			right : "+=" + (remove_width + 6),
			opacity : 0
		}, 100)
	};

}

// Callback function used to collapse a particular section
function collapse_section(){
	$(this).parents(".dropdown").addClass('hide');
	var collapse_btn = $(this);
	var section_content = $(this).parents(".white-block").find(".section-content");

	// hide the menu after it is clicked
	if(section_content.css('display') !== "block"){
		collapse_btn.html('<i class="iconic-fullscreen-exit"></i> Collapse');
	}else{
		collapse_btn.html('<i class="iconic-fullscreen"></i> Expand');
	}

	// Finally hide the section_content
	$(this).parents(".white-block").find(".section-content").slideToggle();
}




//////////////////////////////////// ELEMENT CREATION ZONE /////////////////////////////////////////

// Create and return the confirmation dialog for a question
function create_row_delete_confirmation(row_type){
	return $('<div class="delete-row-confirmation" style="width:100%; height: 90%; background-color:#f5f5f5; z-index:1000; position:absolute;top:2px;left:0; text-align:center;"><span>Really want to delete this ' + row_type + '?&nbsp;&nbsp;</span><button class="btn btn-danger ' + row_type + '-delete-confirm">Yes</button>&nbsp;&nbsp;<button class="btn btn-info ' + row_type + '-delete-cancel">No</button></div>')
}

// Creates a delete challenge button
function create_delete_challenge_btn(){
	return $('<div class="delete-challenge-btn" title="Delete challenge"><div class="btn-group"><button class="btn btn-primary"><i class="iconic-minus"></i></button></div></div>');
}

function create_question_remove_btn(){
	return $('<div class="remove-question-btn" title="Delete question"><div class="btn-group"><button class="btn btn-danger"><i class="iconic-minus"></i></button></div></div>');
}

function create_choice_remove_btn(){
	return $('<div class="remove-choice-btn" title="Delete choice"><div class="btn-group"><button class="btn btn-danger"><i class="iconic-minus"></i></button></div></div>');
}

function create_dilemma_remove_btn(){
	return $('<div class="remove-dilemma-btn" title="Delete dilemma set"><div class="btn-group"><button class="btn btn-danger"><i class="iconic-minus"></i></button></div></div>');
}


//////////////////////////////////////////////////////////////////////////////////////////////////////









////////////////////// CHALLENGE LOGIC ///////////////////////////////////////////////////////////
// Add a new challenge
function add_challenge(){
	var challenge_block = create_challenge_block(this);

	$(challenge_block).on("click",".add-question-btn", add_question);
	autosize_textareas($(challenge_block).find("textarea"));

	$(challenge_block).find(".section-challenge").append(create_delete_challenge_btn());
	$(challenge_block).find(".section-tags").html('<select class="select-tags" multiple="multiple"></select><span class="floating-label" contenteditable="false">Tags</span>');
	$(this).parents("legend").next().append(challenge_block);
	$(challenge_block).on('click', ".delete-challenge-btn",delete_challenge_action);

	$(challenge_block).find('textarea').change(mark_challenge_altered);

	activate_selectize(challenge_block);

}


function delete_challenge_action(){
	$(this).parents(".well").find(".delete-challenge-confirmation").removeClass("hide");
	$(this).parents(".well").find(".delete-challenge-confirmation .btn-danger").click(delete_challenge_action_yes);
	$(this).parents(".well").find(".delete-challenge-confirmation .btn-info").click(delete_challenge_action_no);
}

function delete_challenge_action_yes(){
	$.ajax({
		url : "/plugins/core_hooks/delete_challenge/",
		method : 'post',
		context : this,
		data : {
			data : $(this).parents(".section").attr('id') + ":" + $(this).parents(".well").attr('id'),
			subcase_id : $("#this-subcase-id").val()
		},
		success : function(data){
			if (data === "1") {
				$(this).parents(".well").remove();
				notify("Success", "Challenge successfully deleted.", "success", {nonblock : true});
			}
		},
		error : function(xhr, status, error){
			notify("Error deleting challenge", "Unable to delete challenge, try again in some time.", "danger", {nonblock : true});
		}
	})
	
}

function delete_challenge_action_no(){
	$(this).parents(".delete-challenge-confirmation").addClass("hide");
}

// Create and return a challenge block
function create_challenge_block(selector){
	var wells = $(selector).parents(".section").find(".well:first");
	var well_clone = $(wells[0]).clone().attr('id', 'well_' + get_well_id(selector)).attr('altered','false');

	// Arrange textboxs and clean up the unnecessary questions, dilemmas and choices
	$(well_clone).find("textarea").each(function(){
		
		if (parseInt($(this).height()) > 20) {
			$(this).css("height","20px");
		}
		if ($(this).parent().hasClass("section-question") || $(this).parent().hasClass("section-choice") || $(this).parent().hasClass("section-dilemma")) {
			$(this).parent().remove();
		}else $(this).val('');
	})
	$(well_clone).find(".section-tags div.selectize-control").remove();
	$(well_clone).find(".select-tags").removeClass("selectized").css("display","block");
	return well_clone;
}

// Compute the new well id
function get_well_id(selector){
	return (parseInt($(selector).parents(".section").find(".well:last").attr('id').split("_")[1]) + 1);
}


/////////////////////////////////////////////////////////////////////////////////////////////////

function mark_challenge_altered(){
	$(this).parents(".well").attr("altered",true);
	$("#toolbox-save-subcase,#toolbox-save-answers,#toolbox-save-comments").css('background-color', "#F41C1C");

	// If save is the last child, give it proper border radius
	if ($("#toolbox-save-subcase,#toolbox-save-answers,#toolbox-save-comments").is(":last-child")) $("#toolbox-save-subcase,#toolbox-save-answers,#toolbox-save-comments").css('border-radius', '0 0 4px 0');
	
}




///////////////////////////////// WELL VALIDATIONS //////////////////////////////////////////


// This is a mandatory function and must be provided by all plugins for validation of wells
function validate_well(well){
	// Pack all the validation functions into the hash of following name with section id as their key
	// This is used by save_changed_data for validation before saving data

	var SECTION_VALIDATION_CALLBACKS = {
		'id-read-between-the-lines-section' : validate_read_btn_lines_well,
		'id-design-strategic-solution-section' : validate_design_well,
		'id-select-solution-section' : validate_select_solution_well,
		'id-description-section' : validate_descriptive_well
	}
	return SECTION_VALIDATION_CALLBACKS[$(well).parents(".container.section").attr('id')](well);
}


// Validation function for read between the lines section wells
function validate_read_btn_lines_well(well){

	// id of well rows that need not be filled to pass validation
	var not_required_array = [
		'section_challenge_description',
		'section_tags'
	]
	var flag = true, error_array = new Array();

	$(well).find(".editable-div").each(function(){
		if($.trim($(this).find('textarea').val()) == '' && $.inArray($(this).attr('id'), not_required_array) === -1){
			scrollTo($(this).parents(".well"), -100);
			error_array.push(this);
			flag = false;
		}
	})
	$.each(error_array, function(){
		var prev_color = $(this).css('background-color');
		$(this).css('background-color', "#F41C1C");
		$(this).animate({
			backgroundColor : prev_color
		}, 2000);
	})

	return flag;
}


// Validation function for select solutons section wells
function validate_select_solution_well(well){

	// id of well rows that need not be filled to pass validation
	var not_required_array = [
		'section_challenge_description',
		'section_tags',
	]
	var flag = true, error_array = new Array();

	$(well).find(".editable-div").each(function(){
		if($.trim($(this).find('textarea').val()) == '' && $.inArray($(this).attr('id'), not_required_array) === -1){
			scrollTo($(this).parents(".well"), -100);
			error_array.push(this);
			flag = false;
		}
	})
	$.each(error_array, function(){
		var prev_color = $(this).css('background-color');
		$(this).css('background-color', "#F41C1C");
		$(this).animate({
			backgroundColor : prev_color
		}, 2000);
	})

	return flag;


}


// Validation function for design strategic section wells
function validate_design_well(well){

	// id of well rows that need not be filled to pass validation
	var not_required_array = [
		'section_challenge_description',
		'section_tags',
	]
	var flag = true, error_array = new Array();

	$(well).find(".editable-div").each(function(){
		if($.trim($(this).find('textarea').val()) == '' && $.inArray($(this).attr('id'), not_required_array) === -1){
			scrollTo($(this).parents(".well"), -100);
			error_array.push(this);
			flag = false;
		}
	})
	$.each(error_array, function(){
		var prev_color = $(this).css('background-color');
		$(this).css('background-color', "#F41C1C");
		$(this).animate({
			backgroundColor : prev_color
		}, 2000);
	})

	return flag;

}


// Validation function for descriptive section wells
function validate_descriptive_well(well){

	// id of well rows that need not be filled to pass validation
	var not_required_array = [
		'section_challenge_description',
		'section_tags',
	]
	var flag = true, error_array = new Array();

	$(well).find(".editable-div").each(function(){
		if($.trim($(this).find('textarea').val()) == '' && $.inArray($(this).attr('id'), not_required_array) === -1){
			scrollTo($(this).parents(".well"), -100);
			error_array.push(this);
			flag = false;
		}
	})
	$.each(error_array, function(){
		var prev_color = $(this).css('background-color');
		$(this).css('background-color', "#F41C1C");
		$(this).animate({
			backgroundColor : prev_color
		}, 2000);
	})

	return flag;

}