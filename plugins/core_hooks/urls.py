from django.conf.urls import patterns, include, url


urlpatterns = patterns('plugins.core_hooks.views',
	url(r'^(?P<user_id>\d+)/view_subcase/(?P<subcase_id>\d+)/?$', 'view_subcase'),
    url(r'^view_subcase/(?P<subcase_id>\d+)/?$', 'view_subcase_unauth'),
    url(r'^solve_subcase/(?P<subcase_id>\d+)/?$', 'solve_subcase'),
    url(r'^evaluate_case_study/(?P<case_id>\d+)/?$', 'evaluate_case_study'),
    url(r'^evaluate_answers/(?P<user_id>\d+)/(?P<case_id>\d+)/?$', 'evaluate_answers'),

)

urlpatterns += patterns('plugins.core_hooks.ajax',
	url(r'^add_section/?$', 'add_section'),
	url(r'^save_subcase/?$', 'save_subcase'),
	url(r'^delete_section/?$', 'delete_section'),
	url(r'^delete_challenge/?$', 'delete_challenge'),
	url(r'^fetch_subcase_tags/?$', 'fetch_subcase_tags'),
	url(r'^delete_row/?$', 'delete_row'),
	url(r'^save_answers/?$', 'save_answers'),
	url(r'^save_comments/?$', 'save_comments'),

	url(r'^fetch_challenge_tags/?$', 'fetch_challenge_tags'),
	url(r'^add_challenge_tags/?$', 'add_challenge_tags'),
	url(r'^delete_challenge_tags/?$', 'delete_challenge_tags'),
	

	url(r'^upload_photo_redactor/(?P<case_id>\d+)?/?$', 'upload_photo_redactor'),
	url(r'^fetch_photos_redactor/(?P<num_photo>\d+)?/?$', 'fetch_photos_redactor'),
)

