from django.conf import settings
from django.contrib.auth import *
from core.lib.decorators import login_required, ajax_view
from django.template import RequestContext, loader, Context
from django.http import HttpResponse, HttpResponseRedirect
import json
from .forms import *
from .models import *
from core.models import SubcaseStudy, SubcaseTags, CaseStudyImages
from collections import OrderedDict
from sys import stderr

def get_dilemma_key(data):
	""" 
	Computes the dilemma key since we save dilemma in a special way
	"""
	split_list = data.split("_")
	return split_list[0] + "_" + split_list[-1]




@login_required
def add_section(request):
	if not request.is_ajax():
		return HttpResponse("0")
	
	if('id-select-solution' == request.REQUEST['id_type']):
		t = loader.get_template('part_templates/select_solution_section.html')
		ret_data = t.render(Context({'section_id' : 'id-select-solution-section'}))
	elif('id-read-between-the-lines' == request.REQUEST['id_type']):
		t = loader.get_template('part_templates/read_btn_lines.html')
		ret_data = t.render(Context({'section_id' : 'id-read-between-the-lines-section'}))
	elif('id-design-strategic-solution' == request.REQUEST['id_type']):
		t = loader.get_template('part_templates/design_section.html')
		ret_data = t.render(Context({'section_id' : 'id-design-strategic-solution-section'}))
	elif('id-description' == request.REQUEST['id_type']):
		t = loader.get_template('part_templates/descriptive_section.html')
		ret_data = t.render(Context({'section_id' : 'id-description-section'}))

	return HttpResponse(ret_data)


@ajax_view(SubcaseDataForm)
def save_subcase(request):
	data = request.form.cleaned_data
	subcase_id = int(data['subcase_id'])
	subcase = SubcaseStudy.objects.get(pk=subcase_id, is_active=True)
	incoming_data = json.loads(data['data'])

	

	for section_key, section_value in incoming_data.iteritems():
		for well_key, well_value in section_value.iteritems():
			try:
				# Should challenge_entry be queried using active = True as well... Have to think

				challenge_entry = TeachingCaseChallenge.objects.get(subcase_id = subcase_id, section_key = section_key, well_key = well_key, is_active=True)
				challenge_entry.challenge = well_value['section_challenge']
				challenge_entry.description = well_value['section_challenge_description']
				challenge_entry.save()

				# Stores dilemma keys to check repetitions
				dilemma_keys = []

				# Now save the questions and the choices
				for item_key, item_value in well_value.iteritems():
					# Save questions
					if item_key.startswith("question_"):
						try:
							question_entry = TeachingCaseQuestion.objects.get(challenge_id = int(challenge_entry.id) ,question_key = item_key, is_active=True)
							question_entry.question = item_value
							question_entry.is_active = True
							question_entry.save()
						except Exception, e:
							question_entry = TeachingCaseQuestion(question_key = item_key, question = item_value)
							question_entry.challenge = challenge_entry
							question_entry.is_active = True
							question_entry.save()

						

					# Save choices
					if item_key.startswith("choice_"):
						try:
							choice_entry = TeachingCaseChoice.objects.get(challenge_id = int(challenge_entry.id) ,choice_key = item_key, is_active=True)
							choice_entry.choice = item_value
							choice_entry.challenge = challenge_entry
							choice_entry.is_active = True
							choice_entry.save()
						except Exception, e:
							choice_entry = TeachingCaseChoice(choice_key = item_key, choice = item_value)
							choice_entry.challenge = challenge_entry
							choice_entry.is_active = True
							choice_entry.save()
						
					# Save dilemmas
					if item_key.startswith("dilemma_") and item_key not in dilemma_keys:
						dilemma_number = item_key.split("_")[-1]
						
						try:
							dilemma_entry = TeachingCaseDilemmaAction.objects.get(challenge_id = int(challenge_entry.id) ,dilemma_key = get_dilemma_key(item_key), is_active=True)

							dilemma_entry.dilemma = well_value['dilemma_dilemma_' + dilemma_number]
							dilemma_entry.action = well_value['dilemma_action_' + dilemma_number]
							dilemma_entry.question = well_value['dilemma_question_' + dilemma_number]
							
							dilemma_entry.challenge = challenge_entry
							dilemma_entry.is_active = True
							dilemma_entry.save()

							dilemma_keys.extend(['dilemma_dilemma_' + dilemma_number, 'dilemma_action_' + dilemma_number, 'dilemma_question_' + dilemma_number])
			
						except Exception, e:
							dilemma_entry = TeachingCaseDilemmaAction(dilemma_key = get_dilemma_key(item_key))

							dilemma_entry.dilemma = well_value['dilemma_dilemma_' + dilemma_number]
							dilemma_entry.action = well_value['dilemma_action_' + dilemma_number]
							dilemma_entry.question = well_value['dilemma_question_' + dilemma_number]

							dilemma_entry.challenge = challenge_entry
							dilemma_entry.is_active = True
							dilemma_entry.save()

							dilemma_keys.extend(['dilemma_dilemma_' + dilemma_number, 'dilemma_action_' + dilemma_number, 'dilemma_question_' + dilemma_number])
			
			except Exception, e:
				# Create a new entry into the database
				challenge_entry = TeachingCaseChallenge(subcase_id = subcase_id, section_key = section_key, well_key = well_key, challenge = well_value['section_challenge'], description = well_value['section_challenge_description'], is_active=True)

				# If dilemma and action are present, save them also
				try:
					challenge_entry.dilemma = well_value['section_dilemma']
					challenge_entry.action = well_value['section_action']
				except KeyError:
					pass
				challenge_entry.subcase = subcase
				challenge_entry.save()

				# Stores dilemma keys to check repetitions
				dilemma_keys = []

				# Now save the questions and the choices
				for item_key, item_value in well_value.iteritems():
					# Save questions
					if item_key.startswith("question_"):
						question_entry = TeachingCaseQuestion(question_key = item_key, question = item_value)
						question_entry.challenge = challenge_entry
						question_entry.save()

					# Save choices
					if item_key.startswith("choice_"):
						choice_entry = TeachingCaseChoice(choice_key = item_key, choice = item_value)
						choice_entry.challenge = challenge_entry
						choice_entry.save()

					if item_key.startswith("dilemma_") and item_key not in dilemma_keys:
						dilemma_number = item_key.split("_")[-1]
						dilemma_entry = TeachingCaseDilemmaAction(dilemma_key = get_dilemma_key(item_key))

						dilemma_entry.dilemma = well_value['dilemma_dilemma_' + dilemma_number]
						dilemma_entry.action = well_value['dilemma_action_' + dilemma_number]
						dilemma_entry.question = well_value['dilemma_question_' + dilemma_number]

						dilemma_entry.challenge = challenge_entry
						dilemma_entry.save()

						dilemma_keys.extend(['dilemma_dilemma_' + dilemma_number, 'dilemma_action_' + dilemma_number, 'dilemma_question_' + dilemma_number])
						


	return HttpResponse("1")


@ajax_view(SubcaseDataForm)
def delete_challenge(request):
	data = request.form.cleaned_data
	keys = data['data'].split(":")
	subcase_id = int(data['subcase_id'])
	try:
		challenge_item = TeachingCaseChallenge.objects.get(subcase_id = subcase_id, section_key = keys[0], well_key = keys[1], is_active=True)
		challenge_item.is_active = False
		challenge_item.save()
	except Exception, e:
		pass
	return HttpResponse("1")

@ajax_view(SubcaseDataForm)
def delete_section(request):
	data = request.form.cleaned_data
	subcase_id = int(data['subcase_id'])
	print data
	try:
		challenge_item = TeachingCaseChallenge.objects.filter(subcase_id = subcase_id, section_key = data['data']).update(is_active=False)
	except Exception, e:
		pass
	return HttpResponse("1")

@ajax_view(SubcaseDataForm)
def fetch_subcase_tags(request):
	data = request.form.cleaned_data
	subcase_id = int(data['subcase_id'])
	query = data['data']
	subcase_tags = SubcaseTags.objects.filter(subcase_id = subcase_id, tag_data__icontains = query)
	ret_json = {}
	ret_json['tags'] = [{ 'id' : tag.id, 'name' : tag.tag_data, 'type' : tag.tag_type } for tag in subcase_tags]
	return HttpResponse(json.dumps(ret_json))


@ajax_view(SubcaseDataForm)
def delete_row(request):
	data = request.form.cleaned_data
	ids = data['data'].split(":")
	try:
		challenge_id = TeachingCaseChallenge.objects.only('id').get(subcase_id = int(data['subcase_id']), section_key = ids[0], well_key = ids[1], is_active=True).id
		if ids[2].startswith('question_'):
			question = TeachingCaseQuestion.objects.get(challenge_id = int(challenge_id), question_key = ids[2], is_active=True)
			question.is_active = False
			question.save()
		elif ids[2].startswith('dilemma_'):
			dilemma = TeachingCaseDilemmaAction.objects.get(challenge_id = int(challenge_id), dilemma_key = ids[2].split("_")[0] + "_" + ids[2].split("_")[-1], is_active=True)
			dilemma.is_active = False
			dilemma.save()
		else:
			choice = TeachingCaseChoice.objects.get(challenge_id = int(challenge_id), choice_key = ids[2], is_active=True)
			choice.is_active = False
			choice.save()
	except Exception, e:
		pass

	return HttpResponse("1")


@ajax_view(SubcaseAnswersForm)
def save_answers(request):
	data = request.form.cleaned_data
	subcase_id = int(data['subcase_id'])
	user_id = int(data['user_id'])
	# user = User.objects.get(pk=user_id)
	incoming_data = json.loads(data['data'])
	for challenge_key, challenge_value in incoming_data.iteritems():
		for well_key, well_value in challenge_value.iteritems():
			choices = TeachingCaseChoice.objects.only('id', 'choice_key' ,'status').filter(challenge__subcase_id__exact = subcase_id, challenge__section_key = challenge_key, challenge__well_key = well_key, is_active=True)
			for choice in choices:
				choice.status = True if well_value[choice.choice_key] == u'true' else False
				choice.save()

			questions = TeachingCaseQuestion.objects.only('id', 'question_key').filter(challenge__subcase_id__exact = subcase_id, challenge__section_key = challenge_key, challenge__well_key = well_key, is_active=True)
			for question in questions:
				this_question_answer = well_value["answer_" + question.question_key.split("_")[-1]]
				try:
					answer = TeachingCaseAnswer.objects.get(question_id = question.id, user = request.user)
					answer.answer = this_question_answer
					answer.save()
				except Exception, e:
					answer = TeachingCaseAnswer(question = question, answer = this_question_answer, user = request.user)
					answer.save()

			dilemmas = TeachingCaseDilemmaAction.objects.filter(challenge__subcase_id__exact = subcase_id, challenge__section_key = challenge_key, challenge__well_key = well_key, is_active=True)
			for dilemma in dilemmas:
				dilemma.answer = well_value[ dilemma.dilemma_key.split('_')[0] + '_answer_' + dilemma.dilemma_key.split('_')[-1] ]
				dilemma.save()

	return HttpResponse("1")


@ajax_view(SaveCommentsForm)
def save_comments(request):
	data = request.form.cleaned_data
	user_id = int(data['user_id'])
	incoming_data = json.loads(data['data'])

	for subcase_id, subcase_value in incoming_data.iteritems():
		for section_key, section_value in subcase_value.iteritems():
			for well_key, well_value in section_value.iteritems():
				questions = TeachingCaseQuestion.objects.only('id', 'question_key').filter(challenge__subcase_id__exact = int(subcase_id), challenge__section_key = section_key, challenge__well_key = well_key, is_active=True)
				print len(questions)
				for question in questions:
					this_question_comment = well_value["comment_" + question.question_key.split("_")[-1]]
					try:
						answer = TeachingCaseAnswer.objects.get(question_id = question.id, user = request.user)
						answer.remark = this_question_comment
						answer.save()
					except Exception, e:
						answer = TeachingCaseAnswer(question = question, remark = this_question_comment, user = request.user)
						answer.save()

				# Save the comments on dilemmas
				dilemmas = TeachingCaseDilemmaAction.objects.filter(challenge__subcase_id__exact = int(subcase_id), challenge__section_key = section_key, challenge__well_key = well_key, is_active=True)
				print len(dilemmas)
				for dilemma in dilemmas:
					dilemma.remark = well_value[ dilemma.dilemma_key.split('_')[0] + '_comment_' + dilemma.dilemma_key.split('_')[-1] ]
					dilemma.save()

	return HttpResponse("1")

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST


@csrf_exempt
@require_POST
@login_required
def upload_photo_redactor(request, *args, **kwargs):
	"""
	Upload image files from redactor
	"""
	images = []
	if kwargs['case_id']:
		case_id = int(kwargs['case_id'])
		for img in request.FILES.getlist('file'):
			image = CaseStudyImages.objects.create(image = img, case_id = case_id)
			images.append({'filelink' : image.image.url})
	else:
		pass
	print images
	return HttpResponse(json.dumps(images), mimetype="application/json")

@login_required
def fetch_photos_redactor(request, *args, **kwargs):
	return HttpResponse(json.dumps({'status' : "1"}))


@ajax_view(ChallengeIdentityForm)
def fetch_challenge_tags(request, *args, **kwargs):
	data = request.form.cleaned_data
	subcase_id = int(data['subcase_id'])
	challenge_key = data['challenge_key']
	section_key = data['section_key']
	print data
	response_data = {}
	try:
		challenge = TeachingCaseChallenge.objects.get(subcase_id = subcase_id, well_key = challenge_key, section_key = section_key, is_active=True)

		tags = challenge.tags.all()
		response_data['tags'] = [ {'id' : tag.id, 'data' : tag.tag_data, 'type' : tag.tag_type} for tag in tags]
	except Exception, e:
		pass

	return HttpResponse(json.dumps(response_data))



@ajax_view(ChallengeIdentityForm)
def add_challenge_tags(request, *args, **kwargs):
	data = request.form.cleaned_data
	print data
	subcase_id = int(data['subcase_id'])
	tag_id = int(data['tag_id'])
	section_key = data['section_key']
	challenge_key = data['challenge_key']

	response_data = {}
	try:
		challenge = TeachingCaseChallenge.objects.get(subcase_id = subcase_id, well_key = challenge_key, section_key = section_key, is_active=True)

		if challenge:
			tag = SubcaseTags.objects.get(pk=tag_id, is_active=True)
			challenge.tags.add(tag)
			challenge.save()
		response_data['status'] = "1"
	except Exception, e:
		response_data['status'] = "0"
	
	return HttpResponse(json.dumps(response_data))

@ajax_view(ChallengeIdentityForm)
def delete_challenge_tags(request, *args, **kwargs):
	data = request.form.cleaned_data
	print data
	subcase_id = int(data['subcase_id'])
	tag_id = int(data['tag_id'])
	section_key = data['section_key']
	challenge_key = data['challenge_key']

	response_data = {}
	try:
		challenge = TeachingCaseChallenge.objects.get(subcase_id = subcase_id, well_key = challenge_key, section_key = section_key, is_active=True)

		if challenge:
			tag = SubcaseTags.objects.get(pk=tag_id, is_active=True)
			challenge.tags.remove(tag)
			challenge.save()
		response_data['status'] = "1"
	except Exception, e:
		response_data['status'] = "0"
	
	return HttpResponse(json.dumps(response_data))


