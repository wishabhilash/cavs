from django.conf import settings
import os
import copy
from django.utils.importlib import import_module
from django.utils import six
import sys

class Autodiscover(object):
	"""
	This module autodiscovers INSTALLED_APPS and INSTALLED_PLUGINS and update them on-demand
	"""
	excludes = set([settings.PROJECT_NAME,".git",".",".."])
	
	def __init__(self):
		super(Autodiscover, self).__init__()

		# Gets the list of already installed apps
		# and appends them to exclude list
		# we need not install the same apps again
		self.installed_apps = list(settings.INSTALLED_APPS)
		for a in self.installed_apps:
			self.excludes.add(a)

		# self.installed_plugins = list(settings.INSTALLED_PLUGINS)
		# for a in self.installed_plugins:
		# 	self.excludes.add(a)

	def __install_apps(self, apps):
		""" Helper method to install apps """
		if not isinstance(apps,list):
			raise Exception("Argument must be a list")

		self.installed_apps += apps
		settings.INSTALLED_APPS = tuple(self.installed_apps)

	def apps(self):
		"""
		This function discovers and installs dynamically added apps on demand
		"""
		for path in ('apps', 'plugins'):
			# List all the contents of the project directory
			dir_list = os.listdir(os.path.join(settings.PROJECT_PATH,path))

			# List all the apps ready for installation and append to variable installable_apps
			for fil in dir_list:

				# Checks if the file is a directory and not in our list of excluded files
				if os.path.isdir(os.path.join(path,fil)) and path + "." + fil not in self.excludes:
					try:
						# Import module app_settings
						app_settings = import_module("%s.%s.app_settings" % (path,fil))

						# Import apps and plugins urls.py
						urls_mod = import_module("%s.urls" % path)

						# Install app
						self.__install_apps([path + "." + fil,])

						# Update new urls from newly added app
						urls_mod.urlpatterns += app_settings.URL_PATTERN_APPEND
					except:
						raise

		from django.template.loaders import app_directories
		app_directories.app_template_dirs = tuple(self.__reload_template_dirs())


	def __reload_template_dirs(self):
		"""
		Reloads the templates in the apps
		"""

		if not six.PY3:
			fs_encoding = sys.getfilesystemencoding() or sys.getdefaultencoding()
		app_template_dirs = []
		for app in settings.INSTALLED_APPS:
			try:
				mod = import_module(app)
			except ImportError as e:
				raise ImproperlyConfigured('ImportError %s: %s' % (app, e.args[0]))
			template_dir = os.path.join(os.path.dirname(mod.__file__), 'templates')
			if os.path.isdir(template_dir):
				if not six.PY3:
					template_dir = template_dir.decode(fs_encoding)
				app_template_dirs.append(template_dir)
		return app_template_dirs

	
	def cache_hooks(self):
		"""
		Import all the hooks. Must be called after all the apps are installed.
		"""
		for app in settings.INSTALLED_APPS:
			try:
				hook_mod = import_module(app + ".hooks")
			except ImportError as e:
				pass
