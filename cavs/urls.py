from django.conf.urls import patterns, include, url, static
from django.conf import settings

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'CASconcept.views.home', name='home'),
    # url(r'^CASconcept/', include('CASconcept.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    url(r'^', include('core.urls')),
    url(r'^apps/', include('apps.urls')),
    url(r'^plugins/', include('plugins.urls')),
)


# urlpatterns += autodiscover.urlpatterns()