from django.core.management.base import BaseCommand, CommandError
from django.template import Context, Template
import shutil, os
from django.conf import settings

class Command(BaseCommand):
	""" This command creates a new plugin """
	
	def handle(self, *args, **options):
		if len(args) != 1:
			raise CommandError("Format: ./manage startplugin <plugin_name>")
		context = {"app_name" : args[0]}

		# Copy plugin template to create a new plugin
		shutil.copytree(os.path.join(settings.PROJECT_PATH, "core", "management", "templates", "app"),
			os.path.join(settings.PROJECT_PATH, "apps",context["app_name"]))

		# Open app_settings.py file to read file
		f = open(os.path.join(settings.PROJECT_PATH, "apps",context["app_name"],"app_settings.py"), 'r')
		data = Template(f.read())
		f.close()

		# Write into app_settings.py with the tags replaced
		f = open(os.path.join(settings.PROJECT_PATH, "apps",context["app_name"],"app_settings.py"), 'w')
		f.write(data.render(Context(context)))
		f.close()