from django import template
from core.models import *
from collections import OrderedDict

register = template.Library()

@register.filter
def connect_by_hyphen(value):
	"""
	Converts any string to hyphen connected string in lower case.
	Example: "A B c d" -> "a-b-c-d"
	"""
	split_data = value.lower().split()
	return "-".join(split_data)

@register.filter
def tab_target(value):
	"""
	This is a special function only for use in tags context menu
	which computes the target from context menu button to tab pane
	to add a tag
	"""
	return "-".join(value.split()[2:]) + "tab-pane"


@register.filter
def get_section(value, key):
	return value[key]

@register.filter
def sortdict_by_key(value):
	ret_dict = OrderedDict()
	for key in sorted(value.iterkeys()):
		ret_dict[key] = value[key]
	return ret_dict


@register.filter
def startswith(value, fragment):
	return value.startswith(fragment)


@register.filter
def is_moderator(user_id, case_id):
	user = User.objects.get(pk=int(user_id))
	return len(CaseStudy.objects.filter(moderator = user, pk = int(case_id), is_active=True))


@register.filter
def is_collaborator(user_id, case_id):
	user = User.objects.get(pk=int(user_id))
	return len(CaseStudy.objects.filter(collaborator = user, pk = int(case_id), is_active=True))


@register.filter
def trim(value, delimiter = ' '):
	value = str(value)
	return value.strip(delimiter)