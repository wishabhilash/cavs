from django.conf import settings
from django.contrib.auth import *
from .lib.decorators import login_required, ajax_view
from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import *
from django.http import HttpResponse, HttpResponseRedirect

from django.core.mail import send_mail
from .models import *
import os, json
from django.db.models import Q
from core.lib import library

from sys import stderr

try:
	import smtplib
except Exception, e:
	stderr.write("error import smtplib...")

def register_user(request):
	'''
		Register the User
	'''
	if request.method == 'POST' and request.is_ajax():
		form = SignupForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			try:
				# Insert the data of the user to create an account
				user = User.objects.create_user(username = email,password = password, first_name = first_name, last_name = last_name, email = email)


				# For mail confirmation of the user we must 
				# first deactivate the user on account creation
				# if 'VCAP_SERVICES' in os.environ:
				# 	stderr.write("Inside here...")
				# 	if user.is_active:
				# 		user.is_active = False
				# 		user.save()

				# 	# # Mail the account confirmation link to the registering User
				# 	if not user.is_active:
				# 		from core.lib import tools

				# 		message = "Click on the link below to activate your CAVS account:\nhttp://%s" % request.META['HTTP_HOST'] + "/" + os.path.join('account_activation', tools.hash(user.email + str(user.date_joined).split(".")[0].split("+")[0]),str(int(user.id)))
				# 		try:
				# 			stderr.write("Sending mail...")
				# 			send_mail(
				# 				subject = "Activate your CAVS account",
				# 				message = message,
				# 				from_email = settings.ADMINS[0][1],
				# 				recipient_list = [user.username]
				# 				)
				# 			stderr.write("Mail sent")
				# 			return HttpResponse("success")
				# 		except Exception, e:
				# 			stderr.write("Mail not sent")
				# 			stderr.write(e)
				# else:
				return HttpResponse(json.dumps({'status' : "1"}), mimetype = "application/json")
					
			except Exception:
				raise
	for i,j in form.errors.iteritems():
		print i,j
	form_errors = {}
	form_errors['status'] = "0"
	form_errors['errors'] = form.errors
	return HttpResponse(json.dumps(form_errors), mimetype = "application/json")

def login_user(request):
	if request.method == 'POST' and request.is_ajax():
		form = LoginForm(request.POST)
		if form.is_valid():
			user = authenticate(username = form.cleaned_data['email'], 
				password = form.cleaned_data['password'])
			if user is not None and user.is_active:
				login(request, user)
				user_row = User.objects.get(username = request.user)
				return HttpResponse('/%s/' % str(user_row.id))
	return HttpResponse('failure')



@login_required
def get_subcases(request):
	if request.method == 'POST' and request.is_ajax():
		pass


@ajax_view(CreateCaseStudyForm)
def create_case_study(request):
	'''
		Creates the case study by inserting new values into the DATABASE
	'''
	data = request.form.cleaned_data
	case = CaseStudy(name = data['case_name'], company_name = data['company_name'])
	case.save()

	# Retrieve the User to make him the author/ moderator
	user = User.objects.get(username = str(request.user))

	# Make the user moderator
	case.moderator.add(user)
	return HttpResponse(str(case.id))



@login_required
def fetch_case_studies(request):
	''' Fetch and return all the case studies in which 
		a particular user is either moderator or collaborator
	'''
	if request.method == 'POST' and request.is_ajax():
		cases = CaseStudy.objects.filter(Q(moderator=request.user) | Q(collaborator=request.user), is_active=True)
		json_data = {}
		for case in cases:
			json_data[case.id] = case.name
		return HttpResponse(json.dumps(json_data))


@login_required
def fetch_bookmarks(request):
	''' Fetch and return all the bookmarks the user has created
	'''

	if request.method == 'POST' and request.is_ajax():
		return HttpResponse(json.dumps({'a' : 1, 'b' : 2}))


@login_required
def set_story_name(request):
	try:
		User.objects.get(username = request.user).update(name = request.REQUEST['story_name'])
		return HttpResponse("success")
	except Exception, e:
		return HttpResponse("failure")

@login_required
def set_story(request, *args, **kwargs):
	if request.method == 'POST' and request.is_ajax:
		try:
			# print request.REQUEST['story_data']
			case_data = CaseStudy.objects.get(pk=request.REQUEST['case_id'])
			case_data.story = request.REQUEST['story_data']
			case_data.save()

			return HttpResponse("success")
		except Exception, e:
			return HttpResponse("failure")
		




@login_required
def get_subcase_templates(request, *args, **kwargs):
	if request.POST and request.is_ajax:
		# print json.dumps(library.hook_cache['subcase_hook'][int(request.REQUEST['subcase_id'])].templates)
		return HttpResponse(json.dumps(library.hook_cache['subcase_hook'][request.REQUEST['subcase_id']].templates))
	return HttpResponse("failure")


@ajax_view(CreateSubcaseForm)
def create_subcase(request, *args, **kwargs):
	"""
	Creates a subcase for a certain case
	@args: case_id, subcase_id, template_id, story_context
	"""
	data = request.form.cleaned_data
	return_data = {
		"status" : "",
		"data" : ""
	}
	try:
		subcase = SubcaseStudy(subcase_id=data['subcase_id'], story = data['story_context'], template_id = data['template_id'], name = data['subcase_name'])
		case = CaseStudy.objects.get(pk=int(data['case_id']))
		subcase.case_id = case
		subcase.save()

		# Import tags from case to its subcase
		case_tags = CaseTags.objects.filter(case_id = int(data['case_id']))
		for tag in case_tags:
			subcase_tags = SubcaseTags(tag_data = tag.tag_data, tag_type = tag.tag_type)
			subcase_tags.subcase = subcase
			subcase_tags.save()

		return_data['status'] = '1'
		return_data['data'] = {
			'subcase_id' : subcase.id,
		}
	except Exception, e:
		return_data['status'] = '0'
	
	return HttpResponse(json.dumps(return_data))
	

	


@ajax_view(CheckSubcaseForm)
def check_subcase_name(request, *args, **kwargs):
	data = request.form.cleaned_data
	res = CaseStudy.objects.filter(id = int(data['case_id']),subcasestudy__name__iexact = data['subcase_name'])
	if not res:
		return HttpResponse("1");
	else:
		return HttpResponse("0");


@login_required
def save_case_tags(request, *args, **kwargs):
	if request.is_ajax():
		case_id = request.REQUEST['case_id']
		tag = request.REQUEST['tag']
		key = request.REQUEST['key']
		tag_options_key = "-".join(key.split("-")[:-2]) + "-tag-option"
		print request.REQUEST
		
		case = CaseStudy.objects.get(pk=int(case_id))
		case_tags = CaseTags(tag_data = tag, tag_type = key)
		case_tags.case = case
		case_tags.save()

		try:
			hooks = library.hook_cache[tag_options_key]
			tag_options = [ {'label_name' : str(value), 'classname' : value.get_class_name(), 'callback' : hook.get_js_callback() } for key, value in hooks.iteritems() ]
		except KeyError:
			tag_options = None

		return HttpResponse(json.dumps({"status" : 1, "index" : case_tags.id, 'options' : tag_options}))
	return HttpResponse(json.dumps({'status' : 0}))



def fetch_case_tags(request, *args, **kwargs):
	"""
	Fetch all the case tags for display. Used by case_tags_js.html
	"""
	if request.is_ajax():
		case_id = int(request.REQUEST['case_id'])
		tags = CaseTags.objects.filter(case_id = case_id, is_active=True)
		ret_json = {}
		
		for tag in tags:
			
			try:
				ret_json[tag.tag_type].update({ str(tag.id) : { 'tag_data' : tag.tag_data } })
			except KeyError:
				try:
					options_hook = library.hook_cache["-".join(tag.tag_type.split("-")[0:-2]) + "-tag-option"]
					options = []
					for hook in options_hook.itervalues():
						options.append({'name' : str(hook), 'classname' : hook.get_class_name(), 'callback' : hook.get_js_callback() })
				except KeyError:
					options= None
				
				ret_json[tag.tag_type] = { str(tag.id) : { 'tag_data' : tag.tag_data }, 'options' : options }
	
		return HttpResponse(json.dumps(ret_json))
	return HttpResponse("0")



@login_required
def delete_case_tags(request, *args, **kwargs):
	if request.is_ajax():
		case_id = int(request.REQUEST['case_id'])
		index = request.REQUEST['index']
		key = request.REQUEST['key']
		data = request.REQUEST['data']
		print request.REQUEST
		try:
			tag = CaseTags.objects.get(pk=int(index), tag_data = data, tag_type = key, case_id = case_id)
			tag.is_active = False
			tag.save()
			return HttpResponse("1")
		except Exception, e:
			return HttpResponse("0")

	return HttpResponse("0")


@login_required
def save_subcase_tags(request, *args, **kwargs):
	if request.is_ajax():
		case_id = request.REQUEST['case_id']
		tag = request.REQUEST['tag']
		key = request.REQUEST['key']
		subcase = SubcaseStudy.objects.get(pk=int(case_id))
		subcase_tags = SubcaseTags(tag_data = tag, tag_type = key)
		subcase_tags.subcase = subcase
		subcase_tags.save()
		return HttpResponse(json.dumps({"status" : 1, "index" : subcase_tags.id}))

	return HttpResponse(json.dumps({'status' : 0}))

@login_required
def fetch_subcase_tags(request, *args, **kwargs):
	if request.is_ajax():
		subcase_id = int(request.REQUEST['case_id'])
		tags = SubcaseTags.objects.filter(subcase_id = subcase_id, is_active=True)
		ret_json = {}
		
		for tag in tags:
			
			try:
				ret_json[tag.tag_type].update({ str(tag.id) : { 'tag_data' : tag.tag_data } })
			except KeyError:
				try:
					options_hook = library.hook_cache["-".join(tag.tag_type.split("-")[0:-2]) + "-subcase-tag-option"]
					options = []
					for hook in options_hook.itervalues():
						options.append({'name' : str(hook), 'classname' : hook.get_class_name(), 'callback' : hook.get_js_callback() })
				except KeyError:
					options= None
				
				ret_json[tag.tag_type] = { str(tag.id) : { 'tag_data' : tag.tag_data }, 'options' : options }
	
		return HttpResponse(json.dumps(ret_json))
	return HttpResponse("0")

@login_required
def delete_subcase_tags(request, *args, **kwargs):
	if request.is_ajax():
		case_id = int(request.REQUEST['case_id'])
		index = request.REQUEST['index']
		key = request.REQUEST['key']
		data = request.REQUEST['data']
		try:
			SubcaseTags.objects.get(pk=int(index), tag_data = data, tag_type = key, subcase_id = case_id).delete()
			return HttpResponse("1")
		except Exception, e:
			return HttpResponse("0")
	return HttpResponse("0")


@ajax_view(GetCollaboratorsForm, method = 'GET')
def get_collaborators(request, *args, **kwargs):
	'''
	Returns the collaborators for search in Collaborators box in Edit Case Study
	'''
	data = request.form.cleaned_data
	
	users = User.objects.filter(Q(first_name__icontains = data['query']) | Q(last_name__icontains = data['query']))
	json_ret = {}
	json_ret['users'] = [{'id' : int(user.id),'full_name' : "%s %s" % (user.first_name, user.last_name), 'email' : user.username} for user in users if not user.username == str(request.user)]

	# display_pic = UserProfile.objects.get(pk=int(user.id)).profile_pic
		
	print json.dumps(json_ret)
	return HttpResponse(json.dumps(json_ret))

@ajax_view(AddCollaboratorForm)
def remove_collaborator(request, *args, **kwargs):
	case_id = int(request.form.cleaned_data['case_id'])
	user_id = int(request.form.cleaned_data['user_id'])
	print request.form.cleaned_data
	user = User.objects.get(pk=user_id)
	case = CaseStudy.objects.get(pk=case_id, is_active=True)
	case.collaborator.remove(user)
	case.save()
	return HttpResponse("1")


@ajax_view(AddCollaboratorForm)
def add_collaborator(request, *args, **kwargs):
	case_id = int(request.form.cleaned_data['case_id'])
	user_id = int(request.form.cleaned_data['user_id'])
	user = User.objects.get(pk=user_id)
	case = CaseStudy.objects.get(pk=case_id, is_active=True)
	case.collaborator.add(user)
	case.save()
	return HttpResponse("1")


@ajax_view(AddCollaboratorForm)
def add_moderator(request, *args, **kwargs):
	case_id = int(request.form.cleaned_data['case_id'])
	user_id = int(request.form.cleaned_data['user_id'])
	user = User.objects.get(pk=user_id)
	case = CaseStudy.objects.get(pk=case_id)
	case.moderator.add(user)
	case.save()
	return HttpResponse("1")




@ajax_view(AddCollaboratorForm)
def add_collaborator_remove_moderator(request, *args, **kwargs):
	case_id = int(request.form.cleaned_data['case_id'])
	user_id = int(request.form.cleaned_data['user_id'])
	user = User.objects.get(pk=user_id)
	case = CaseStudy.objects.get(pk=case_id)
	case.collaborator.add(user)
	case.moderator.remove(user)
	case.save()
	return HttpResponse("1")


@ajax_view(AddCollaboratorForm)
def add_moderator_remove_collaborator(request, *args, **kwargs):
	case_id = int(request.form.cleaned_data['case_id'])
	user_id = int(request.form.cleaned_data['user_id'])
	user = User.objects.get(pk=user_id)
	case = CaseStudy.objects.get(pk=case_id)
	case.moderator.add(user)
	case.collaborator.remove(user)
	case.save()
	return HttpResponse("1")


@ajax_view(GetCollaboratorsForm)
def fetch_collaborators(request, *args, **kwargs):
	'''
	Returns the collaborators for list in view case study
	'''

	case_id = int(request.form.cleaned_data['query'])
	case = CaseStudy.objects.get(pk=case_id)
	collaborators = case.collaborator.all()

	json_ret = {}
	json_ret['collaborators'] = [{'id' : int(user.id),'full_name' : "%s %s" % (user.first_name, user.last_name), 'email' : user.username} for user in collaborators]
	json_ret['status'] = '1'
	return HttpResponse(json.dumps(json_ret))


@ajax_view(GetCollaboratorsForm)
def fetch_collaborators_and_moderators(request, *args, **kwargs):
	case_id = int(request.form.cleaned_data['query'])
	case = CaseStudy.objects.get(pk=case_id)
	collaborators = case.collaborator.all()
	moderators = case.moderator.all()

	json_ret = {}
	json_ret['collaborators'] = [{'id' : int(user.id),'full_name' : "%s %s" % (user.first_name, user.last_name), 'email' : user.username} for user in collaborators]
	json_ret['moderators'] = [{'id' : int(user.id),'full_name' : "%s %s" % (user.first_name, user.last_name), 'email' : user.username} for user in moderators]
	json_ret['status'] = '1'
	return HttpResponse(json.dumps(json_ret))


@ajax_view(GetCollaboratorsForm)
def delete_case_study(request, *args, **kwargs):
	data = request.form.cleaned_data
	case_id = int(data['query'])

	case = CaseStudy.objects.get(pk=case_id, is_active=True)
	case.is_active = False
	case.save()
	return HttpResponse("1")


@login_required
def save_case_general_settings(request, *args, **kwargs):
	from datetime import datetime
	import dateutil.parser as dateparser
	
	if request.method == "POST" and request.is_ajax():
		data = request.REQUEST
		print data
		case_id = int(data['case_id'])
		deadline = dateparser.parse(data['deadline']).strftime("%Y-%m-%d %H:%M:%S")
		case_name = data['case_name']
		publish = True if data['publish'] == 'yes' else False
		company_name = data['company_name']

		case = CaseStudy.objects.get(pk=case_id, is_active=True)
		case.name = case_name
		case.is_published = publish
		case.company_name = company_name
		case.save()

	return HttpResponse("1")


@ajax_view(GlobalSearchForm)
def global_search(request, *args, **kwargs):
	data = request.form.cleaned_data
	query = data['query']
	query_type = data['query_type']
	print data
	if query_type == 'all':
		total_result = []
		for value in library.hook_cache['search_entry_hook'].itervalues():
			value_str = "_".join(str(value).lower().split())
			total_result.append({value_str: value.get_results(query, 3)})
	else:
		hook = library.hook_cache['search_entry_hook'][query_type]
		hook_str = "_".join(str(hook).lower().split())
		total_result = []
		print hook.get_results(query, 20)
		total_result.append({ hook_str : hook.get_results(query, 20)})
	return HttpResponse(json.dumps(total_result))

@ajax_view(CaseTagActorDetailsForm)
def save_case_tags_actor_details(request, *args, **kwargs):
	data = request.form.cleaned_data
	print data
	tag_id = int(data['tag_id'])
	name = data['name']
	company = data['company']
	email = data['email']
	tag_data = data['data']
	phone = data['phone']

	ret_dict = {}

	case_tag = CaseTags.objects.get(pk=tag_id)
	case_tag.tag_data = tag_data
	case_tag.save()
	try:
		actor_info = CaseTagActorInfo.objects.get(case_tag_id=tag_id)
		actor_info.email = email
		actor_info.name = name
		actor_info.company = company
		actor_info.phone = phone
		actor_info.save()
		ret_dict['status'] = "1"
	except Exception, e:
		CaseTagActorInfo(case_tag=case_tag, name = name, phone = phone, email = email, company = company).save();
		ret_dict['status'] = "0"
	return HttpResponse(json.dumps(ret_dict))



@ajax_view(GetCollaboratorsForm)
def fetch_case_tags_actor_details(request, *args, **kwargs):
	data = request.form.cleaned_data
	tag_id = int(data['query'])
	ret_dict = {}
	try:
		actor_info = CaseTagActorInfo.objects.get(case_tag_id=tag_id)
		ret_dict['name'] = actor_info.name
		ret_dict['company'] = actor_info.company
		ret_dict['email'] = actor_info.email
		ret_dict['phone'] = actor_info.phone
		ret_dict['data'] = CaseTags.objects.only('tag_data').get(pk=tag_id).tag_data
		ret_dict['status'] = "1"

	except Exception, e:
		ret_dict['data'] = CaseTags.objects.only('tag_data').get(pk=tag_id).tag_data
		ret_dict['status'] = "0"
	return HttpResponse(json.dumps(ret_dict))



@ajax_view(CaseTagActorDetailsForm)
def save_subcase_tags_actor_details(request, *args, **kwargs):
	data = request.form.cleaned_data
	print data
	tag_id = int(data['tag_id'])
	name = data['name']
	company = data['company']
	email = data['email']
	tag_data = data['data']
	phone = data['phone']

	ret_dict = {}

	subcase_tag = SubcaseTags.objects.get(pk=tag_id)
	subcase_tag.tag_data = tag_data
	subcase_tag.save()
	try:
		actor_info = SubcaseTagActorInfo.objects.get(subcase_tag_id=tag_id)
		actor_info.email = email
		actor_info.name = name
		actor_info.company = company
		actor_info.phone = phone
		actor_info.save()
		ret_dict['status'] = "1"
	except Exception, e:
		SubcaseTagActorInfo(subcase_tag=subcase_tag, name = name, phone = phone, email = email, company = company).save();
		ret_dict['status'] = "0"
	return HttpResponse(json.dumps(ret_dict))



@ajax_view(GetCollaboratorsForm)
def fetch_subcase_tags_actor_details(request, *args, **kwargs):
	data = request.form.cleaned_data
	tag_id = int(data['query'])
	ret_dict = {}
	try:
		actor_info = SubcaseTagActorInfo.objects.get(subcase_tag_id=tag_id)
		ret_dict['name'] = actor_info.name
		ret_dict['company'] = actor_info.company
		ret_dict['email'] = actor_info.email
		ret_dict['phone'] = actor_info.phone
		ret_dict['data'] = SubcaseTags.objects.only('tag_data').get(pk=tag_id).tag_data
		ret_dict['status'] = "1"

	except Exception, e:
		ret_dict['data'] = SubcaseTags.objects.only('tag_data').get(pk=tag_id).tag_data
		ret_dict['status'] = "0"
	return HttpResponse(json.dumps(ret_dict))