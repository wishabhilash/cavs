from django import forms
import re
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    # TODO: Define form fields here
    email = forms.CharField(widget=forms.TextInput(attrs={'id' : 'id-login-email','name':'login_email','class' : 'validate[required, custom[email]]', 'placeholder' : 'Enter Email'}), label='')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'id' : 'id-login-password','name':'login_password','class' : 'validate[required]', 'placeholder' : 'Enter Password',  }), label='')

class SignupForm(forms.Form):
    # TODO: Define form fields here
    first_name = forms.CharField(widget=forms.TextInput(attrs={'name':'login_email','class' : 'validate[required]', 'placeholder' : 'First Name'}), label='')
    
    last_name = forms.CharField(widget=forms.TextInput(attrs={'name':'login_email','class' : 'validate[required]', 'placeholder' : 'Last Name'}), label='')
    
    email = forms.CharField(widget=forms.TextInput(attrs={'id' : 'id-signup-email','name':'login_email','class' : 'validate[required, custom[email]]', 'placeholder' : 'Your Email'}), label='')
    
    confirm_email = forms.CharField(widget=forms.TextInput(attrs={'id' : 'id-confirm-email','name':'login_email','class' : 'validate[required, custom[email]]', 'placeholder' : 'Re-enter Email'}), label='')
    
    password = forms.CharField(widget=forms.PasswordInput(attrs={'id' : 'id-signup-password','name':'login_password','class' : 'validate[required]', 'placeholder' : 'Enter Password'}), label='')

    def clean_email(self):
        patt = r'^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$'
        email = self.cleaned_data['email']
        if not re.match(patt, email):
            raise forms.ValidationError("Email is not apt")
        try:
            User.objects.get(username = email)
        except Exception, e:
            return self.cleaned_data['email']
        raise ValidationError("User already exists.")
            
        
    
    def clean_first_name(self):
        patt_alpha = r'^[A-Za-z]+$'
        first_name = self.cleaned_data['first_name']
        if not re.match(patt_alpha, first_name):
            raise forms.ValidationError("Can't have special characters.")
        return first_name

    def clean_last_name(self):
        patt_alpha = r'^[A-Za-z]+$'
        last_name = self.cleaned_data['last_name']
        if not re.match(patt_alpha, last_name):
            raise forms.ValidationError("Can't have special characters.")
        return last_name

    def clean_password(self):
        length = 8
        password = self.cleaned_data['password']
        patt_caps = r'.*[A-Z]+.*'
        patt_small = r'.*[a-z]+.*'
        patt_num = r'.*[0-9]+.*'
        patt_spl_chars = r'.*[._,`~!$\&\^\(\)]+.*'

        if len(password) < length:
            raise forms.ValidationError("Password length must be greater than 7 character")
        if not re.match(patt_caps, password):
            raise forms.ValidationError("Password must have atleast one capital letter.")
        if not re.match(patt_small, password):
            raise forms.ValidationError("Password must have atleast one small letter.")
        if not re.match(patt_num, password):
            raise forms.ValidationError("Password must have atleast one number.")
        if not re.match(patt_spl_chars, password):
            raise forms.ValidationError("Password must have atleast one special character from <._,`~!$&^() >.")
        return password

class CreateCaseStudyForm(forms.Form):
	case_name = forms.CharField(widget=forms.TextInput(attrs={'name':'case_study_name','class' : 'validate[required]', 'placeholder' : 'Case Study Name'}), label='')
	company_name = forms.CharField(widget=forms.TextInput(attrs={'name':'case_study_company_name','class' : 'validate[required]', 'placeholder' : 'Company Name'}), label='')
    
		
class SetStoryForm(forms.Form):
    story = forms.CharField()
    
class GetCollaboratorsForm(forms.Form):
    query = forms.CharField()
    
class CreateSubcaseForm(forms.Form):
    case_id = forms.CharField()
    subcase_id = forms.CharField()
    subcase_name = forms.CharField()
    template_id = forms.CharField()
    story_context = forms.CharField()
    
class CheckSubcaseForm(forms.Form):
    case_id = forms.CharField()
    subcase_name = forms.CharField()


class AddCollaboratorForm(forms.Form):
    case_id = forms.CharField()
    user_id = forms.CharField()


class SaveCaseGeneralSettingsForm(forms.Form):
    case_id = forms.CharField()
    case_name = forms.CharField()
    publish = forms.CharField()
    deadline = forms.CharField()
    company_name = forms.CharField()

class GlobalSearchForm(forms.Form):
    query = forms.CharField()
    query_type = forms.CharField()


class CaseTagActorDetailsForm(forms.Form):
    tag_id = forms.CharField()
    name = forms.CharField(required=False)
    data = forms.CharField(required=False)
    company = forms.CharField(required=False)
    email = forms.CharField(required=False)
    phone = forms.CharField(required=False)