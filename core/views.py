from django.conf import settings
from django.contrib.auth import *
from .lib.decorators import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import *
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from plugins.core_hooks.models import *
from core.lib import library
from django.views.decorators.csrf import csrf_exempt
import re
# Create your views here.


@csrf_exempt
@redirect_if_logged_in
def welcomepage(request):
	return render_to_response('frontpage.html', {'login_form' : LoginForm(), 'signup_form' : SignupForm()},context_instance = RequestContext(request))


def _404(request):
	return render_to_response("_404.html",context_instance = RequestContext(request))


def logout_user(request):
	logout(request)
	return HttpResponseRedirect('/')
	# return render_to_response('frontpage.html', {'login_form' : LoginForm(), 'signup_form' : SignupForm()},context_instance = RequestContext(request))


def activate_account(request, *args, **kwargs):
	from core.lib import tools
	user = User.objects.get(pk=int(kwargs['user_id']))
	if tools.hash(user.email + str(user.date_joined).split("+")[0]) == str(kwargs['activation_id']):
		user.is_active = True
		user.save()
		return HttpResponseRedirect("/")
	else:
		return HttpResponseRedirect("/errorpage/")


@login_required
@auth_required
def homepage(request, user_id):
	return render_to_response('homepage.html', {'create_case_form' : CreateCaseStudyForm(), 'user_id' : user_id}, context_instance = RequestContext(request))

@login_required
def edit_case_study(request, *args, **kwargs):
	from core.lib import tools

	case_id = int(kwargs['case_id'])
	case_data = CaseStudy.objects.get(pk=case_id)
	subcases = SubcaseStudy.objects.filter(case_id__id = case_id, is_active = True)

	context = {
	'case_name' : case_data.name,
	'case_story' : case_data.story,
	'subcases' : subcases,
	# Use case_tags_obj if tags are from CaseStudy model
	'case_tags_obj' : [hook_obj for hook_obj in library.hook_cache['tags_hook'].itervalues()]
	}

	kwargs.update(context)

	pattern = r'[\w]+-tag-option'
	store_key = set()
	for hook_key in library.hook_cache.iterkeys():
		if re.match(pattern,hook_key):
			for hook_element_value in library.hook_cache[hook_key].itervalues():
				for i in hook_element_value.get_js_paths():
					store_key.add(i)

	kwargs['case_tags_hooks_js'] = list(store_key)
	# print context['tags_obj'].get_section_template()
	return render_to_response('edit_case_study.html', kwargs, context_instance = RequestContext(request))


@login_required
def create_subcase(request,*args, **kwargs):
	user_id = User.objects.only('id').get(username = str(request.user)).id
	case_data = CaseStudy.objects.get(pk=kwargs['case_id'])
	kwargs['case_story'] = case_data.story

	# Get Subcase Hooks
	kwargs['subcase_hooks'] = [{'name': str(hook), 'hook_id': hook.get_id()} for hook in library.hook_cache['subcase_hook'].itervalues()]

	return render_to_response("create_subcase.html", kwargs, context_instance = RequestContext(request))


@login_required
def edit_subcase(request, *args, **kwargs):
	# Retrieve the subcase

	subcase = SubcaseStudy.objects.get(id = int(kwargs['subcase_id']), is_active=True)
	subcase_hooks = library.hook_cache['subcase_hook'][subcase.subcase_id]

	# Fetch challenges that are active
	subcase_sections = TeachingCaseChallenge.objects.filter(subcase_id = int(kwargs['subcase_id']), is_active = True)
	output = {}
	for section in subcase_sections:
		well_values = {
			'section_challenge' : section.challenge,
			'section_description' : section.description,
		}

		# Get the Questions
		questions = TeachingCaseQuestion.objects.filter(challenge_id = section.id, is_active = True)
		for question in questions:
			well_values[question.question_key] = question.question


		# Get the Choices
		choices = TeachingCaseChoice.objects.filter(challenge_id = section.id, is_active = True)
		for choice in choices:
			well_values[choice.choice_key] = [choice.choice,choice.status]

		
		# Get the dilemmas
		dilemmas = TeachingCaseDilemmaAction.objects.filter(challenge_id = section.id, is_active = True)
		for dilemma in dilemmas:
			well_values[dilemma.dilemma_key] = [dilemma.dilemma, dilemma.action, dilemma.question, dilemma.answer, dilemma.remark]

		try:
			output[section.section_key].update({section.well_key : well_values})
		except KeyError:
			output[section.section_key] = {section.well_key : well_values}
	

	kwargs['subcase_name'] = subcase.name
	kwargs['subcase_data'] = output
	kwargs['case_id'] = subcase.case_id_id
	kwargs['story'] = subcase.story
	kwargs['js_includes'] = subcase_hooks.get_js_paths()
	kwargs['css_includes'] = subcase_hooks.get_css_paths()
	kwargs['sections'] = subcase_hooks.templates[subcase.template_id]['include']

	# Get all the sections
	kwargs['all_sections'] = {}
	for value in subcase_hooks.templates.itervalues():
		kwargs['all_sections'].update(value['include'])

	# Retrieve and pass templates associated with this subcase to template
	kwargs['section_menu_items'] = [section_menu_item['name'] for section_menu_item in subcase_hooks.templates.itervalues() if section_menu_item['include'] != kwargs['sections']]
	
	kwargs['subcase_tags_obj'] = [hook_obj for hook_obj in library.hook_cache['subcase_tags_hook'].itervalues()]

	return render_to_response("edit_subcase.html", kwargs, context_instance = RequestContext(request))

from django.views.decorators.csrf import ensure_csrf_cookie

@ensure_csrf_cookie
def view_case_study(request, *args, **kwargs):
	case_id = int(kwargs['case_id'])

	case_data = CaseStudy.objects.get(pk=case_id)
	subcases = SubcaseStudy.objects.filter(case_id__id = case_id, is_active = True)
	kwargs['collaborators'] = {}

	kwargs['collaborators']['collaborators'] = [ collaborator.first_name + " " + collaborator.last_name for collaborator in case_data.collaborator.all()]
	kwargs['collaborators']['moderators'] = [ moderator.first_name + " " + moderator.last_name for moderator in case_data.moderator.all()]

	kwargs['case_id'] = case_data.id
	kwargs['view_mode'] = True
	kwargs['case_name'] = case_data.name
	kwargs['is_published'] = case_data.is_published
	kwargs['story'] = case_data.story
	kwargs['company_name'] = case_data.company_name
	kwargs['subcases'] = subcases
	# Use case_tags_obj if tags are from CaseStudy model
	kwargs['case_tags_obj'] = [hook_obj for hook_obj in library.hook_cache['tags_hook'].itervalues()]

	return render_to_response("view_case_study.html", kwargs, context_instance = RequestContext(request))


@login_required
def view_user_profile(request, *args, **kwargs):
	try:
		user_profile = UserProfile.objects.get(user = request.user)
		kwargs['user_profile'] = user_profile
	except Exception, e:
		pass
	
	return render_to_response('user_profile.html', kwargs, context_instance = RequestContext(request))
