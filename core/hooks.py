from core.lib.hooks import *
from core.lib.library import Library
from django.conf import settings
from core.models import CaseStudy

library = Library()


class CaseStudySearchHook(SearchHook):
	"""docstring for CaseStudySearchHook"""

	_icon = 'iconic-document-alt-stroke'

	def __init__(self):
		super(CaseStudySearchHook, self).__init__()
	
	def __str__(self):
		return u'Case Study'

	def get_results(self, query, num_of_results):
		try:
			cases = CaseStudy.objects.only('id','name', 'description').filter(name__icontains = query, is_active=True, is_published=True)[num_of_results]
		except IndexError:
			cases = CaseStudy.objects.only('id','name', 'description').filter(name__icontains = query, is_active=True, is_published=True)
		return [ {'name' : case.name,'link' : '/view_case_study/' + str(int(case.id)), 'desc' : case.description} for case in cases]


class ActorTagOptionEditInfo(TagObjectOptionHook):
	"""docstring for ActorTagOptionEditInfo"""

	_hook_key = "actor-tag-option"
	_classname = "actor_edit_info"
	_js_callback = 'actor_edit_callback'

	def __init__(self):
		super(ActorTagOptionEditInfo, self).__init__()
		
	def __str__(self):
		return "Edit"



library.register(CaseStudySearchHook)
library.register(ActorTagOptionEditInfo)