from exceptions import *

hook_cache = {}


class Library(object):
	"""This class registers the hooks into global __hook_cache.
	__hook_cache is a dynamic cache that can be used to dynamically add hooks
	"""

	id_dict = {}

	def __init__(self):
		super(Library, self).__init__()

	def register(self, arg):
		# Check if arg is of type "type"
		if not isinstance(arg,type):
			raise Exception("Hook not of type \"type\"")

		global hook_cache
		mod = arg()

		# Check if the id of the hook object is already registered
		# if not update new hook else raise Exception
		try:
			if mod.get_id() not in self.id_dict[mod.get_hook_key()]:
				self.id_dict[mod.get_hook_key()].append(mod.get_id())
			else:
				raise Exception("Hook with the name \"%s\" already present." % str(mod))
		except KeyError:
			self.id_dict[mod.get_hook_key()] = [mod.get_id(),]

		# If no exceptions are raised, update the hook_cache with the now
		# hook object
		try:
			hook_cache[mod.get_hook_key()].update({mod.get_id() : mod})
		except KeyError:
			hook_cache[mod.get_hook_key()] = {mod.get_id() : mod}


