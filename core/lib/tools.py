import hashlib
import os
from django.conf import settings

def hash(string):
	return hashlib.sha1(string).hexdigest()

if __name__ == '__main__':
	print hash("a")