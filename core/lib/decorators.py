from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect

# from django.contrib.auth.models import User
from core.models import *

def login_required(func, login_url = None):
	"""
		This decorator can be used for views to redirect a user to login page
		if he/she is not logged in.
	"""
	def wrap(request, *args, **kwargs):
		if not request.user.is_authenticated():
			if request.is_ajax():
				return HttpResponse(status=404)
			else:

				return HttpResponseRedirect(login_url or settings.LOGIN_URL)
		return func(request, *args, **kwargs)
	return wrap

def auth_required(func, redirect_url = None):
	"""
		This decorator can be used for views to redirect a user to 404 page
		if he/she is not authorized to see the page.
	"""
	def wrap(request, *args, **kwargs):
		user_data = User.objects.get(username = request.user)
		if int(user_data.id) != int(kwargs['user_id']):
			return HttpResponseRedirect(redirect_url or settings._404_ERROR_URL)
		return func(request, *args, **kwargs)
	return wrap

def redirect_if_logged_in(func, redirect_url = None):
	"""
		This decorator can be used for views to redirect a user to home page
		if he/she is already logged in.
	"""
	def wrap(request, *args, **kwargs):
		if request.user.is_authenticated():
			user_data = User.objects.get(username = request.user)
			return HttpResponseRedirect(redirect_url or "/%s/" % str(user_data.id))
		return func(request, *args, **kwargs)
	return wrap


# def case_perm(func):
# 	"""
# 		This decorator checks if the user has the proper permissions
# 		to have access to a particular case study to edit
# 	"""
# 	def wrap(request, *args, **kwargs):
# 		p = CaseStudy.objects.filter(User__pk=args['user_id'])
# 		return func(request, *args, **kwargs)
# 	return wrap


from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseForbidden, HttpResponseBadRequest
from django.utils.safestring import mark_safe
import json
from functools import wraps

_ERROR_MSG = '<!DOCTYPE html><html lang="en"><body><h1>%s</h1><p>%%s</p></body></html>'
_400_ERROR = _ERROR_MSG % '400 Bad Request'
_403_ERROR = _ERROR_MSG % '403 Forbidden'
_405_ERROR = _ERROR_MSG % '405 Not Allowed'

def ajax_view(FormClass=None, method='POST', login_required=True, ajax_required=True, json_form_errors=False):
	def decorator(view_func):
		def _ajax_view(request, *args, **kwargs):
			
			# if request.method != method and method != 'REQUEST':
			if request.method != method:
				return HttpResponseNotAllowed(mark_safe(_405_ERROR % ('Request must be a %s.' % method)))

			if ajax_required and not request.is_ajax():
				return HttpResponseForbidden(mark_safe(_403_ERROR % 'Request must be set via AJAX.'))

			if login_required and not request.user.is_authenticated():
				return HttpResponseForbidden(mark_safe(_403_ERROR % 'User must be authenticated!'))

			if FormClass:
				f = FormClass(getattr(request, method))
				if not f.is_valid():
					if json_form_errors:
						errors = dict((k, [unicode(x) for x in v]) for k,v in f.errors.items())
						return HttpResponse(json.dumps({'error': 'form', 'errors': errors}), 'application/json')
					else:
						return HttpResponseBadRequest(mark_safe(_400_ERROR % ('Invalid form<br />' + f.errors.as_ul())))
				request.form = f
			return view_func(request, *args, **kwargs)
		return wraps(view_func)(_ajax_view)
	return decorator


class auth_required_view_case(object):
	"""
	Decorator that checks if user is 
	"""
	def __init__(self, redirect_url):
		self.redirect_url = redirect_url

	def __call__(self, func):
		def wrapper(request, *args, **kwargs):
			try:
				self.redirect_url += kwargs['case_id']
			except KeyError:
				return HttpResponseRedirect(settings._404_ERROR_URL)
			try:
				if request.user.is_authenticated():
					user_data = User.objects.get(username = request.user)
					if int(user_data.id) != int(kwargs['user_id']):
						return HttpResponseRedirect(self.redirect_url or settings._404_ERROR_URL)
				else:
					return HttpResponseRedirect(self.redirect_url or settings._404_ERROR_URL)
			except KeyError, DoesNotExist:
				return HttpResponseRedirect(self.redirect_url or settings._404_ERROR_URL)
			return func(request, *args, **kwargs)

		return wrapper

class auth_required_view_subcase(auth_required_view_case):
	"""
	Decorator that checks if user is 
	"""
	def __init__(self, redirect_url):
		self.redirect_url = redirect_url

	def __call__(self, func):
		def wrapper(request, *args, **kwargs):
			try:
				self.redirect_url += kwargs['subcase_id']
			except KeyError:
				return HttpResponseRedirect(settings._404_ERROR_URL)
			try:
				if request.user.is_authenticated():
					user_data = User.objects.get(username = request.user)
					if int(user_data.id) != int(kwargs['user_id']):
						return HttpResponseRedirect(self.redirect_url or settings._404_ERROR_URL)
				else:
					return HttpResponseRedirect(self.redirect_url or settings._404_ERROR_URL)
			except KeyError, DoesNotExist:
				return HttpResponseRedirect(self.redirect_url or settings._404_ERROR_URL)
			return func(request, *args, **kwargs)

		return wrapper


def authorize_only_moderators(func):
	def wrapper(request, *args, **kwargs):
		user = User.objects.get(username = str(request.user))
		case = CaseStudy.objects.filter(moderator = user, pk=int(kwargs['case_id']))
		if not case:
			return HttpResponseRedirect(settings._404_ERROR_URL)
		return func(request, *args, **kwargs)
	return wrapper

def authorize_only_collaborators(func):
	def wrapper(request, *args, **kwargs):
		user = User.objects.get(username = str(request.user))
		case = CaseStudy.objects.filter(collaborator = user, pk=int(kwargs['case_id']))
		if not case:
			return HttpResponseRedirect(settings._404_ERROR_URL)
		return func(request, *args, **kwargs)
	return wrapper
