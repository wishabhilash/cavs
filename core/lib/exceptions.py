class ClassInstantiationException(Exception):
	"""docstring for ClassInstantiationException"""
	def __init__(self):
		super(ClassInstantiationException, self).__init__("Class must remain uninstantiated.")
		