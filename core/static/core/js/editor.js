$(function(){
	$("#save-button").hide();
	$("#editor-space").addClass("background-white");

	// Activated when Save button is clicked
	$("#save-button").click(function(){
		if(EDITOR_AJAX_FLAG){
			var save_data = $("#editor").redactor('get');
			$.ajax({
				url : "/set_story/",
				type : 'post',
				data : {
					story_data : getElementData("#editor"),
					case_id : "{{ case_id }}"
				},
				success : function(data){
					console.log(data);
					if (data === 'success'){
						notify("Update successful", "Your story has been updated successfully.", "success", {nonblock : true});
					}else{
						notify("Update unsuccessful", "Story update was unsuccessful.", "error", {nonblock : true});
					}
				},
				error : function(xhr, status, error){
					notify("Update unsuccessful", error + " Story update was unsuccessful.", "error", {nonblock : true});
				}
			});
		}
		
		$(this).hide();
		$("#edit-button").show();
		destroyEditor("#editor");
		$("#editor-space").addClass("background-white");
		$("#editor").addClass("savemode").addClass("tag-support").removeClass("editmode");

	});

	// Activated when Edit button is clicked
	// It displays the editor and the save button while hiding itself
	$("#edit-button").click(function(){
		$("#editor-space").removeClass("background-white");
		$(this).hide();
		$("#save-button").show();
		$("#editor").addClass("editmode").removeClass("savemode").removeClass("tag-support");

		// Add Autosave features below to other_opts to activate autosave feature
		// autosave : "/save_story/",
  //   	autosaveInterval : 120,
  //   	autosaveCallback : function(data){
    		
    	// }

		var other_opts = {
			imageUpload : "{% url upload_photo_redactor %}",
        	imageGetJson : "/plugins/core_hooks/fetch_photos_redactor/"

		}
		createEditor("#editor", other_opts);
	});

})