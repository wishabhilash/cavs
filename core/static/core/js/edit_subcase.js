$(function(){
	$("body").mouseup(function(event){
		hideSectionContextMenu(event);
	});


	// Autosave every 30 seconds
	var refresh_id = setInterval(function(){
		$("#toolbox-save-subcase,#toolbox-save-answers, #toolbox-save-comments").trigger("click");
	}, 30000);

	// Activate save subcase
	$("#toolbox-save-subcase").click(save_changed_data);

	// Reveal Context menu to add section
	$("#id-add-section").parent().click(function(){
		if(!section_menu_flag){
			var offset = $("#id-add-section").offset();
			var width = $("#id-add-section").parent().width();
			var contextmenu = $("#id-contextmenu-template").clone();

			$(contextmenu).removeClass("hide").attr("id","id-section-menu").css({'top' : "50%", 'left' : (offset.left + width) + "px", 'position' : "fixed", "margin-top" : 30-toolbox_height/2 + "px"});
			$("body").append(contextmenu);
			section_menu_flag = true;
			
			// BIND ALL THE EVENTS TO THE NEW SECTION

			// Bind event to click on context menu item
			$("#id-section-menu").on('click','li',ajax_add_section);

			$("li.disabled").click(return_true);


		}

	});

	// Disable the menus in Add Section which are currently present
	$(".section").each(function(){
		var data_id = $(this).attr('id');
		if(data_id !== '') {
			$("#id-contextmenu-template").find("#" + data_id.slice(0,-8)).parent().addClass('disabled').click(return_false);
		}
	})




	// Collapse toggle all the sections on clicks
	$("#collapse-all").click(function(){
		var content_section = $(".collapse-section").parents(".white-block").find(".section-content");

		// By default the callback in slideToggle is called as many times
		// as the number of elements if receives. However, we want our logic
		// to be executed only once. So initiate flag = false
		var flag = false;
		content_section.slideToggle(function(){
			if(content_section.css("display") == "none" && !flag){
				flag = true;
				$("#collapse-all i").removeClass("iconic-fullscreen-exit").addClass("iconic-fullscreen");
				$("#collapse-all span span").html("Expand All");
			}else if(content_section.css("display") != "none" && !flag){
				flag = true;
				$("#collapse-all i").removeClass("iconic-fullscreen").addClass("iconic-fullscreen-exit");
				$("#collapse-all span span").html("Collapse All");
			}
		});
	});



	// Collapse toggle section on click
	$(".collapse-section").click(collapse_section);

	// Use sortable to provide the ability of rearranging sections
	$("#section-wrapper").sortable({
		revert : true,
		handle : ".handle",
		opacity : 0.7,
		// placeholder : "sortable-placeholder",
		start : function(){
			if ($("#collapse-all").find("i").hasClass("iconic-fullscreen-exit")) $("#collapse-all").trigger("click");
		},
		stop : function(){
			if ($("#collapse-all").find("i").hasClass("iconic-fullscreen")) $("#collapse-all").trigger("click");
		},
	});	

	// Click to reveal the dropdown for each section
	$(".iconic-cog").click(reveal_section_context_menu);
	

})


// Do ajax to retrive a section from server and add it to DOM
function ajax_add_section(){
	var id_type = $(this).find('a').attr("id");
	$.ajax({
		url : "/plugins/core_hooks/add_section/",
		method : 'post',
		data : {
			id_type : id_type,
		},
		success : function(data){
			if(data !== "0"){
				if($("#" + $(data).attr('id')).length === 0){
					var data_id = $(data).attr('id')
					$("#section-wrapper").append($(data));
					scrollTo("#" + $(data).attr('id'),0);
					$("#" + id_type).parent().addClass("disabled");

					// Add section specific modal to body
					var section_delete_modal = $("#delete-section-confirmation-modal").clone().attr('id',data_id + "-modal");
					$("body").append(section_delete_modal);


					// When mouse enters a question the following events occur
					$("#" + data_id + " .section-content").on('mouseenter', '.well div',mouse_enter_row)

					// When mouse leaves a question the following events occur
					$("#" + data_id + " .section-content").on('mouseleave', '.well div',mouse_leave_row);

					// On click - beside question row, show confirm dialog
					$("#" + data_id + " .remove-question-btn").on('click','button.btn-danger',show_question_delete_confirm_dialog);

					// Add a question when + is clicked
					$("#" + data_id + " .add-question-btn").on('click','button.btn-success',add_question);

					// Add a choice when + is clicked
					$("#" + data_id).on('click',".add-choice-btn",add_choice);


					// On click - beside choice row, show confirm dialog
					$("#" + data_id + " .remove-choice-btn").on('click','button.btn-danger',show_choice_delete_confirm_dialog);

					// On click - beside dilemma row, show confirm dialog
					$("#" + data_id + " .remove-dilemma-btn").on('click','button.btn-danger',show_dilemma_delete_confirm_dialog);

					// Add a dilemma set when + is clicked
					$("#" + data_id + " .add-dilemma-btn").on('click','button.btn-success',add_dilemma);




					// Collapse toggle section on click
					$("#" + data_id).on('click',".collapse-section",collapse_section);

					// Add cog-menu item <Add Challenge>
					$("#" + data_id + " .sectioncontrol-btns-small li").append('<a href="javascript:void(0)" role="menuitem" class="add-challenge" title="Add Challenge"><i class="iconic-plus"></i> Challenge</a>');

					// Add cog-menu item <Delete section>
					$("#" + data_id + " .sectioncontrol-btns-small li").append('<a href="#' + data_id + "-modal" + '" role="menuitem" class="delete-section" title="Delete Section" data-toggle="modal"><i class="iconic-minus"></i> Delete Section</a>');

					$("#" + data_id + "-modal").find(".modal-footer .btn-danger").click(delete_section_action_yes);
					
					// Activate all add_challenge buttons by default
					$("#" + data_id + " .add-challenge").click(add_challenge);

					// Click to reveal the dropdown for each section
					$("#" + data_id +" .iconic-cog").click(reveal_section_context_menu);

					autosize_textareas("#" + data_id + " .section-content .well textarea");

					// Activate the change to alter state of wells inside this section
					$("#" + data_id + ' textarea').change(mark_challenge_altered);

					// Activate support for tagging
					$("#" + data_id + " .well").each(function(){
						activate_selectize(this);
					})
					

				}
				$("#id-section-menu").addClass("hide");

			}
			else notify("Error", "Error fetching data", "error", {nonblock:true})
		},
		error : function(xhr, status, error){
			notify("Error", "Error fetching data", "error", {nonblock:true})
		}
	})

}


// If clicked anywhere except context menu and add section button, remove add section menu from DOM
function hideSectionContextMenu(event){
	var container = $("#id-section-menu");
	var section_btn = $("#id-add-section").parent();
	
	if(section_btn.is(event.target) && section_btn.has(event.target).length === 0){
	 return false;
	}
	
	if(!container.is(event.target) && container.has(event.target).length === 0){
		container.remove();
		section_menu_flag = false;
	}
	
}



// Save the altered subcase using ajax
function save_changed_data(){
	var json_data = {};
	var validation_flag = true;
	var altered_wells = $(".section[id$='-section']").not("#story-section");
	if (!$(altered_wells).find(".well[altered='true']").length) return false;
	$(altered_wells).each(function(){
		var well = $(this).find(".well[altered='true']");

		// Get section_id
		var section_id = $(this).attr('id');
		json_data[section_id] = {};

		
		$(well).each(function(){

			// If validation_flag is already false there is no need to go ahead
			if (!validation_flag) return false;

			// If validation fails break the loop and set validation flag to false
			// validate_well() function must be defined in the plugin. There can not be multiple function with the same name. Must only and only be one defined per plugin.
			// For example look at its implementation in /plugins/core_hooks/static/js/core_hooks.js
			if (!validate_well(this)){
				validation_flag = false;
				return false;
			}

			// Insert well id
			var well_id = $(this).attr('id');
			json_data[section_id][well_id] = {};

			var textarea = $(this).find("textarea").each(function(){
				json_data[section_id][well_id][$(this).parent().attr('id')] = $(this).val();	
			});

			
		})

		
	})
	console.log(JSON.stringify(json_data));
	if (!validation_flag) return false;

	$.ajax({
		url : "/plugins/core_hooks/save_subcase/",
		method : 'post',
		data : {
			data : JSON.stringify(json_data),
			subcase_id : $("#this-subcase-id").val()
		},
		success : function(data){
			notify("Success", "Subcase successfully saved.", "success", {nonblock:true});
			$(altered_wells).find(".well[altered='true']").each(function(){
				// Mark the well as altered = false
				$(this).attr('altered','false');
			})
			$("#toolbox-save-subcase").css('background-color', "");
		},
		error : function(xhr, status, error){
			notify("Error saving data", "Some error is preventing from saving the data. Try again in some time.", "danger", {nonblock:true});
		}
	})

	
}

// Reveal the cog-menu
function reveal_section_context_menu(){
	$(this).next().css("position",'absolute').toggleClass("hide");
}
