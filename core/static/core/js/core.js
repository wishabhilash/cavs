(function($){
    // Plugin for error messages
    $.fn.error_msg = function(options){
        var settings = $.extend({
            message : "Some error here",
            backgroundColor : "#971E1E",
            borderColor : "#446E07",
            borderWidth : 1,
            pushRight : 0,
            pushDown : 0,
            opacity : 0.9,
            clearAll : false
        },options)

        // Use this option only on submit of form
        if(settings.clearAll){
            $(".error_msg_plugin").remove();
            return this;
        }

        var offset = this.offset();
        var container = $("<span></span>").addClass('error_msg_plugin').css({
            'display' : 'block',
            'position' : 'absolute',
            'z-index' : '100000',
            'background-color' : settings.backgroundColor,
            'opacity' : settings.opacity,
            'border' : settings.borderWidth + "px solid " + settings.borderColor,
            'border-radius' : "2px",
            'color' : "#fff",
            'padding' : '2px 4px 2px 4px'
        }).text(settings.message);
        container.css({
            'top' : offset.top + settings.pushDown,
            'left' : offset.left + this.width() + settings.pushRight
        })
        $("body").append(container);

        return this;
    }
})(jQuery)



// Given a selector, creates a redactor editor on it
function createEditor(selector, other_opts){
    var opts = {
        linebreaks : true,
        tabSpaces: 4,
        autoresize : false,
        focus: true,
    }

    if (typeof(other_opts) === 'undefined') other_opts = {};
    for(key in other_opts){
        if(key in opts) continue;
        opts[key] = other_opts[key];
    }

    $(selector).redactor(opts);

}

// Given a selector, destroys the redactor editor on it
function destroyEditor(selector){
    $(selector).redactor('destroy');
}


// Get highlighted text
function getSelectionHtml() {
    var html = "";
    if (typeof window.getSelection != "undefined") {
        var sel = window.getSelection();
        if (sel.rangeCount) {
            var container = document.createElement("div");
            for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                container.appendChild(sel.getRangeAt(i).cloneContents());
            }
            html = container.innerHTML;
        }
    } else if (typeof document.selection != "undefined") {
        if (document.selection.type == "Text") {
            html = document.selection.createRange().htmlText;
        }
    }
    return html;
}


function capsFirstLetter(str){
    return str.toLowerCase().replace(/\b[a-z]/g, function(letter){
        return letter.toUpperCase();
    });
}


// var getStackTrace = function() {
//     var obj = {};
//     Error.captureStackTrace(obj, getStackTrace);
//     return obj.stack;
// };


/*  Show/hide the selector menu
    @parameters: selector: its the selector (preferably id of menu)
                 x: the x-pos
                 y: the y-pos

*/
function showContextMenu(selector, x, y){
    $(selector).css({'top' : y +"px", 'left' : x +"px", 'display' : 'block'});
}

function hideContextMenu(selector){
    $(selector).css('display',"none");
}


// function makeContext(selector){
//     return $(selector).bind("contextmenu",function(event){
//         selected_text = getSelectionHtml();
//         showContextMenu(event.pageX, event.pageY);
//         return false;
//     });    
// }




// Scroll to any ID in the page
function scrollTo(selector, topgap, speed){
    if(speed === 'undefined') speed = 2000;
    if(topgap === 'undefined') topgap = 0;

    $('html, body').animate({
       scrollTop: $(selector).offset().top + topgap
    }, speed);
}

// Remove the pnotify history pull down which gets attached to the top
$.pnotify.defaults.history = false;

// Set the default notification area position
var stack_bottomright = {"dir1": "down", "dir2": "left", "firstpos1": 60, "firstpos2": 25};

// Function used to set notifications
function notify(title, text, type, other_opts) {
    var opts = {
        title: title,
        text: text,
        stack: stack_bottomright
    };
    if (typeof(other_opts) === 'undefined') other_opts = {};
    for(key in other_opts){
        if(key in opts) continue;
        opts[key] = other_opts[key];
    }

    switch (type) {
        case 'error':
        opts.type = "error";
        break;
        case 'info':
        opts.type = "info";
        break;
        case 'success':
        opts.type = "success";
        break;
    }
    $.pnotify(opts);
};


// IMPORTANT for Django CSRF protection =============================
function sameOrigin(url) {
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
    (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
    // or any other URL that isn't scheme relative or absolute i.e relative.
    !(/^(\/\/|http:|https:).*/.test(url));
}


function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
//====================================================================



// These are the global options for the spinner
var SPINNER_OPTS = {
    radius: 60,
    height: 20,
    width: 6,
    dashes: 20,
    opacity: 1,
    padding: 3,
    rotation: 300,
    color: '#fff'
}

// Helper function to return the html content inside an element
function getElementData(selector){
    return $.trim($(selector).html());
}


function return_true(){
    return true;
}

function return_false(){
    return false;
}

