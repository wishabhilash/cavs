from django.conf.urls import patterns, include, url
import os
from django.conf import settings

# All site pattern here except ajax
urlpatterns = patterns('core.views',
	url(r'^$', 'welcomepage'),
    url(r'^logout/?$', 'logout_user'),
    url(r'^errorpage/?$', '_404'),
    url(r'^(?P<user_id>\d+)/?$', 'homepage'),
    url(r'^edit_case_study/(?P<case_id>\d+)/?$', 'edit_case_study'),
    url(r'^edit_subcase/(?P<subcase_id>\d+)/?$', 'edit_subcase'),
    url(r'^create_subcase/(?P<case_id>\d+)/?$', 'create_subcase'),
    url(r'^account_activation/(?P<activation_id>[\d\w]+)/(?P<user_id>\d+)/?$', 'activate_account'),

    url(r'^view_case_study/(?P<case_id>\d+)/?$', 'view_case_study'),
    url(r'^user_profile/?$', 'view_user_profile'),


)

# All ajax pattern here
urlpatterns += patterns('core.ajax',
	url(r'^register/?$', 'register_user'),
	url(r'^login/?$', 'login_user'),
	url(r'^create_case_study/?$', 'create_case_study'),
	url(r'^fetch_case_studies/?$', 'fetch_case_studies'),
	url(r'^fetch_bookmarks/?$', 'fetch_bookmarks'),
	
	url(r'^set_story_name/?$', 'set_story_name'),
	url(r'^set_story/?$', 'set_story'),
	
	url(r'^get_collaborators/?$', 'get_collaborators'),
	url(r'^add_collaborator/?$', 'add_collaborator'),
	url(r'^remove_collaborator/?$', 'remove_collaborator'),
	url(r'^fetch_collaborators/?$', 'fetch_collaborators'),

	url(r'^add_moderator/?$', 'add_moderator'),
	
	url(r'^add_moderator_remove_collaborator/?$', 'add_moderator_remove_collaborator'),
	url(r'^add_collaborator_remove_moderator/?$', 'add_collaborator_remove_moderator'),
	
	url(r'^fetch_collaborators_and_moderators/?$', 'fetch_collaborators_and_moderators'),
	
	url(r'^get_subcase_templates/?$', 'get_subcase_templates'),
	url(r'^create_subcase/?$', 'create_subcase'),
	url(r'^check_subcase_name/?$', 'check_subcase_name'),

	url(r'^save_case_tags/?$', 'save_case_tags'),
	url(r'^fetch_case_tags/?$', 'fetch_case_tags'),
	url(r'^delete_case_tags/?$', 'delete_case_tags'),
	url(r'^save_subcase_tags/?$', 'save_subcase_tags'),
	url(r'^fetch_subcase_tags/?$', 'fetch_subcase_tags'),
	url(r'^delete_subcase_tags/?$', 'delete_subcase_tags'),


	url(r'^delete_case_study/?$', 'delete_case_study'),
	url(r'^save_case_general_settings/?$', 'save_case_general_settings'),


	url(r'^global_search/?$', 'global_search'),

	url(r'^save_case_tags_actor_details/?$', 'save_case_tags_actor_details'),
	url(r'^fetch_case_tags_actor_details/?$', 'fetch_case_tags_actor_details'),

	url(r'^save_subcase_tags_actor_details/?$', 'save_subcase_tags_actor_details'),
	url(r'^fetch_subcase_tags_actor_details/?$', 'fetch_subcase_tags_actor_details'),

)


urlpatterns += patterns('',
	(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
)
    