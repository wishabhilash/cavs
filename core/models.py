from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.http import urlquote
from django.conf import settings
from datetime import datetime
from core.lib import tools
import os


def upload_and_rename_profile_pic(instance, filename):
	path = os.path.join(settings.MEDIA_ROOT, tools.hash(instance.username), 'images')
	ext = filename.split('.')[-1]
	new_filename = tools.hash(instance.email).replace('/','') + '.' + ext
	print os.path.join(path, new_filename)
	return os.path.join(path, new_filename)

def upload_and_rename_image(instance, filename):
	path = os.path.join('case_study_images')
	ext = filename.split('.')[-1]
	new_filename = tools.hash(filename) + '.' + ext
	print os.path.join(path, new_filename)
	return os.path.join(path, new_filename)


class UserProfile(models.Model):
	user = models.ForeignKey(User)
	profile_pic = models.ImageField(upload_to=os.path.join(settings.MEDIA_URL,'case_study_images'), blank=True, default="")
	gender = models.CharField(max_length=10)
	about_me = models.CharField(max_length=300, blank=True)
	location = models.CharField(max_length=200)

	class Meta:
		verbose_name = _('UserProfile')
		verbose_name_plural = _('UserProfiles')

	def __unicode__(self):
		return "Profile of user: %s %s" % (self.user.first_name, self.user.first_name)

   
class CaseStudy(models.Model):
	moderator = models.ManyToManyField(User, related_name='moderator')
	collaborator = models.ManyToManyField(User, related_name='collaborator')
	name = models.CharField(max_length=200)
	company_name = models.CharField(max_length=200, default='')
	description = models.CharField(max_length=300, default='')
	story = models.TextField(default = "")
	is_active = models.BooleanField(default=True)
	is_published = models.BooleanField(default=False)

	class Meta:
		verbose_name = _('CaseStudy')
		verbose_name_plural = _('CaseStudies')

	def __unicode__(self):
		return self.name

class CaseStudyImages(models.Model):
	case = models.ForeignKey(CaseStudy)
	image = models.ImageField(upload_to=upload_and_rename_image, blank=True, default="", max_length=400)

	class Meta:
		verbose_name = _('CaseStudyImages')
		verbose_name_plural = _('CaseStudyImagess')

	def __unicode__(self):
		pass
    


class SubcaseStudy(models.Model):
	case_id = models.ForeignKey(CaseStudy)
	subcase_id = models.CharField(max_length=300, default="")
	template_id = models.CharField(max_length=300, default="")
	story = models.TextField(default = "")
	name = models.CharField(max_length = 100)
	is_active = models.BooleanField(default=True)
	deadline = models.DateTimeField(default=datetime.now)
	is_published = models.BooleanField(default=False)

	class Meta:
		verbose_name = _('SubcaseStudy')
		verbose_name_plural = _('SubcaseStudys')

	def __unicode__(self):
		self.name
   

class CaseTags(models.Model):
	case = models.ForeignKey(CaseStudy)
	tag_data = models.TextField()
	tag_type = models.CharField(max_length=50)
	is_active = models.BooleanField(default=True)

	class Meta:
		verbose_name = _('CaseTags')
		verbose_name_plural = _('CaseTagss')

	def __unicode__(self):
		return self.tag_data


class SubcaseTags(models.Model):
	subcase = models.ForeignKey(SubcaseStudy)
	tag_data = models.TextField()
	tag_type = models.CharField(max_length=50)
	is_active = models.BooleanField(default=True)

	class Meta:
		verbose_name = _('SubcaseTags')
		verbose_name_plural = _('SubcaseTagss')

	def __unicode__(self):
		return self.tag_data
    
class CaseTagActorInfo(models.Model):
	case_tag = models.ForeignKey(CaseTags)
	name = models.CharField(max_length=200, default='')
	# pic = models.ImageField(upload_to=upload_and_rename, blank=True, default="")
	company = models.CharField(max_length=200, default='')
	email = models.CharField(max_length=200, default='')
	phone = models.CharField(max_length=15, default='')

	class Meta:
		verbose_name = _('CaseTagInfo')
		verbose_name_plural = _('CaseTagInfos')

	def __unicode__(self):
		return self.case_tags__tag_data


class SubcaseTagActorInfo(models.Model):
	subcase_tag = models.ForeignKey(SubcaseTags)
	name = models.CharField(max_length=200, default='')
	# pic = models.ImageField(upload_to=upload_and_rename, blank=True, default="")
	company = models.CharField(max_length=200, default='')
	email = models.CharField(max_length=200, default='')
	phone = models.CharField(max_length=15, default='')

	class Meta:
		verbose_name = _('SubcaseTagInfo')
		verbose_name_plural = _('SubcaseTagInfos')

	def __unicode__(self):
		return self.case_tags__tag_data

    

######################### Signals ################################

def create_user_dir(sender, **kwargs):
	"""
	This signal is fired when a new user is created (i.e. a new entry into User table)
	It creates a personal directory for the user in user_data directory using sha1 hash of
	newly created users username aka email
	"""
	user = kwargs['instance']
	from core.lib import tools
	import os
	# Create personal directory of the user for his/her contents
	if not os.path.isdir(os.path.join(settings.MEDIA_ROOT, tools.hash(user.username), 'images')):
		os.makedirs(os.path.join(settings.MEDIA_ROOT, tools.hash(user.username), 'images'), mode=0777)



models.signals.post_save.connect(create_user_dir, User, dispatch_uid = "create_user_dir_1")