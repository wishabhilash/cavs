import os
from django.contrib.auth.models import User
from core.lib import library

def cavs_context(request):
	if request.user.is_authenticated():
		user = User.objects.get(username__exact=str(request.user))
		user_id = user.id
		user_fname = user.first_name
		user_lname = user.last_name
	else:
		user_id = None
		user_fname = None
		user_lname = None
		user_obj = None


	
	return {
		"VCAP_SERVICES" : True if "VCAP_SERVICES" in os.environ else False,
		"user_id" : user_id,
		"user_fname" : user_fname,
		"user_lname" : user_lname,
		"global_search_hooks" : library.hook_cache['search_entry_hook']
	}