Requirements:

To run this project you must meet the following requirements:
	
	1. Python 2.7 or above
	2. Django 1.5 or above
	3. Mysql with python-mysql package installed. (Actually any relational database will do)
	4. Git 1.7 or above

For starters:
	
	Clone the repo: git clone https://wishabhilash@bitbucket.org/wishabhilash/cavs.git

This will create a new directory "cavs" in your current working directory.

Patterns followed:

Two very prominent patterns have been followed in this project.

	1. MVC (Model View Controller)
		This is inherently provided by Django.
	2. Plugin architecture:
		This architecture has been developed for absolute decoupling of the codes. Ideally in this project all you need to do is to create a plugin or an app and drop them in their directories. It will seamlessly integrate with the project.
		You can refer to cavs/autodiscover.py for its implementation.


Create database tables:

Go to cavs/settings.py . Change the DATABASES datastructure (in else part), to provide the credentials.
	
	else:
	    DATABASES = {
	        "default": {
	            "ENGINE": "django.db.backends.mysql",  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
	            "NAME": "cavs",  					# Or path to database file if using sqlite3.
	            "USER": "username", 				# Not used with sqlite3.
	            "PASSWORD": "password", 			# Not used with sqlite3.
	            "HOST": "",							# Set to empty string for localhost. Not used with sqlite3.
	            "PORT": "",							# Set to empty string for localhost. Not used with sqlite3.
	            }
	        }


Plugins and Apps:

In this project plugins and apps essentially perform the same functions and are technically identical. However, they are categorized for sake of convenience.

Create new plugin:
	
	cd cavs
	./manage startplugin <plugin-name>

See the new plugin in plugins/ directory

Create new app:
	
	cd cavs
	./manage startnewapp <plugin-name>

See the new plugin in apps/ directory



Adding tag infrastructure

Tagging consists of:

1. Context Menu

2. Tag section with a tabbed interface with each tab being a target for an item in the menu.

They come in two flavours:

1. Case Tags

2. Subcase tags

Implementation doc:
Case tags (see core/templates/edit_case_study.html):
	
	Include this css:
	<link rel="stylesheet" type="text/css" href="{{ STATIC_URL }}core_hooks/css/core_hooks.css">

	Just after the body paste this, it will render the context menu as an hidden entity
	{% include 'part_templates/tags/case_tags_context_menu.html' %}

	This will render the tags section where all the tags will be populated. So put it inside section-wrapper
	{% include 'part_templates/tags/case_tags_section.html' %}

	Lastly, put this at the end of the html file (just before </html>). This will render the required JS.
	{% include 'part_templates/tags/case_tags_js.html' with row_id=case_id %}

Subcase tags (see core/templates/edit_subcase.html):

	Include this css:
	<link rel="stylesheet" type="text/css" href="{{ STATIC_URL }}core_hooks/css/core_hooks.css">

	Just after the body paste this, it will render the context menu as an hidden entity
	{% include 'part_templates/tags/subcase_tags_context_menu.html' %}

	This will render the tags section where all the tags will be populated. So put it inside section-wrapper
	{% include 'part_templates/tags/subcase_tags_section.html' %}

	Lastly, put this at the end of the html file (just before </html>). This will render the required JS.
	{% include 'part_templates/tags/subcase_tags_js.html' with row_id=case_id %}

To enable context menu to tagging you must also include the class "tag-support" on the element. This will make the element qualify for tagging.

	<div class="tag-support">
		Text here
	</div>


Hooks:

Hooks are an integral part of the plugin architecture and are used to seamlessly integrate an app or a plugin.

Presently there are three kind of hooks that are supported, but more can easly be written without much fuss.

1. SubcaseHook

	A subcase hook can be easy created by subclassing SubcaseHook.

		from core.lib.hooks import SubcaseHook
		from core.lib.library import Library
		from django.conf import settings


		class MyHook2(SubcaseHook):
		"""docstring for MyHook"""

		_js_paths = [settings.STATIC_URL + "core_hooks/js/core_hooks.js",]  		# (a)
		stylesheets = [settings.STATIC_URL + "core_hooks/css/core_hooks.css"] 		# (b)

		templates = [																# (c)
			{
				'name' : "Design strategic Solution",
				'include' : {
					"id-design-strategic-solution-section" : "part_templates/design_section.html",
				}
			},
			{
				'name' : "Read between the lines",
				'include' : {
					"id-read-between-the-lines-section" : "part_templates/read_btn_lines.html",
				}
			},
			{
				'name' : "Description",
				'include' : {
					"id-description-section" : "part_templates/descriptive_section.html",
				}
			},
			{
				'name' : "Select Solution",
				'include' : {
					"id-select-solution-section" : "part_templates/select_solution_section.html",
				}
			},
		]

		def __init__(self):
			super(MyHook2, self).__init__()

		def __str__(self): 															# (d)
			return u"Teaching"
	
	library.register(MyHook2) 														# (e)

This is an example showing the implementation of "Teaching Case" plugin (named core_hooks in the project, but it can be named anything).

This hook can be seen in action in "Create Subcase" page. In the above:

	(a) All the javascript files you created for the plugin can be passed here, they will be included in the "Create Subcase page"

	(b) Similar to javascript file, all the css files can be provided here.

	(c) templates: 	'name' creates a "case type"

					'include' : You must provide an 'id' element that defines this case type and an html template that will provide the interface for interaction.

	(d) def __str__(self): This is a *MUST* requirement, and the name itself should be unique. This will provide the name to the plugin.

	(e) Finally register the hook.

2. Tags hooks

Tag hook provide item for tag context menu and tabs in tags-blocks.

In our teaching case plugin, we have a tag named "Actor". The following is its implementation.

	from core.lib.hooks import SubcaseTagsHook
	from core.lib.library import Library
	from django.conf import settings

	class ActorTab(TagsHook):

		def __init__(self):
			super(ActorTab, self).__init__()

		def __str__(self):
			return u"Actor"
	
	library.register(ActorTab)

3. Subcase Tags hooks:

This is the same as for tags, just for subcases.
This will be all you need to create a new tab and tagging support to your project.