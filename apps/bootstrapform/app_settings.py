from django.conf.urls import patterns, include, url

APP_NAME = "bootstrapform"

# This is the list of apps this plugin depends on
# The apps on which it depends must be registed before this app/plugin
DEPENDENCIES = []


URL_PATTERN_APPEND = patterns('',
	url(r'^%s/' % APP_NAME, include('apps.%s.urls' % APP_NAME)),
)
